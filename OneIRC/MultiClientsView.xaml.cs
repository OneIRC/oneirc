﻿namespace OneIRC
{
    #region
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;

    using OneIRC.IRC;

    #endregion

    public sealed partial class MultiClientsView : UserControl
    {
        #region Constructors and Destructors
        public MultiClientsView()
        {
            this.InitializeComponent();

            this.MainGrid.ItemsSource = ((App)Application.Current).ConnectedClients;
        }
        #endregion

        #region Public Properties
        public IRCClient ActiveClient { get; private set; }
        #endregion

        #region Public Methods and Operators
        public void ClientSelectionChangedHandler(object sender, SelectionChangedEventArgs args)
        {
            if (this.MainGrid.Items == null)
            {
                return;
            }

            foreach (var item in this.MainGrid.Items)
            {
                var presenter = this.MainGrid.ContainerFromItem(item) as ContentPresenter;

                if (presenter != null)
                {
                    if (args.AddedItems.Contains(presenter.DataContext))
                    {
                        this.ActiveClient = presenter.DataContext as IRCClient;
                        presenter.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        presenter.Visibility = Visibility.Collapsed;
                    }
                }
            }
        }
        #endregion
    }
}