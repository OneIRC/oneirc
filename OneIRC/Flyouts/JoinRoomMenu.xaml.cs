﻿namespace OneIRC
{
    #region
    using Windows.System;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Controls.Primitives;
    using Windows.UI.Xaml.Input;

    #endregion

    public sealed partial class JoinRoomMenu : UserControl
    {
        #region Static Fields
        public static readonly DependencyProperty ClientViewProperty = DependencyProperty.Register(
                                                                                                   "ClientView",
                                                                                                   typeof(
                                                                                                       MultiClientsView),
                                                                                                   typeof(JoinRoomMenu),
                                                                                                   null);
        #endregion

        #region Constructors and Destructors
        public JoinRoomMenu()
        {
            this.InitializeComponent();
        }
        #endregion

        #region Public Properties
        public MultiClientsView ClientView
        {
            get { return (MultiClientsView)this.GetValue(ClientViewProperty); }
            set { this.SetValue(ClientViewProperty, value); }
        }
        #endregion

        #region Methods
        private void JoinRoomClick(object sender, TappedRoutedEventArgs e)
        {
            if (this.ClientView?.ActiveClient != null)
            {
                this.ClientView.ActiveClient.Join(this.RoomNameTextBox.Text, this.RoomPasswordTextBox.Text);

                this.RoomNameTextBox.Text = string.Empty;
                this.RoomPasswordTextBox.Text = string.Empty;

                var parent = (this.Parent as FlyoutPresenter)?.Parent as Popup;
                if (parent != null)
                {
                    parent.IsOpen = false;
                }
            }
        }

        private void JoinRoomKeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                this.JoinRoomClick(sender, new TappedRoutedEventArgs());
            }
        }
        #endregion
    }
}