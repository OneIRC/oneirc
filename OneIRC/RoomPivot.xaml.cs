﻿namespace OneIRC
{
    #region
    using System;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Threading.Tasks;

    using Windows.UI.Core;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Controls.Primitives;
    using Windows.UI.Xaml.Input;

    using OneIRC.IRC;

    #endregion

    public sealed partial class RoomPivot : UserControl
    {
        #region Static Fields
        public static readonly DependencyProperty ClientProperty = DependencyProperty.Register(
                                                                                               "Client",
                                                                                               typeof(IRCClient),
                                                                                               typeof(RoomPivot),
                                                                                               new PropertyMetadata(
                                                                                                   null,
                                                                                                   OnClientChanged));
        #endregion

        #region Constructors and Destructors
        public RoomPivot()
        {
            this.InitializeComponent();

            var selector = new CustomDataTemplateSelector
                               {
                                   PrivateChannelTemplate = this.PrivateTemplate,
                                   GlobalChannelTemplate = this.GlobalTemplate,
                                   RawChannelTemplate = this.RawTemplate
                               };

            this.Rooms.ItemTemplateSelector = selector;
        }
        #endregion

        #region Public Properties
        public IRCClient Client
        {
            get { return (IRCClient)this.GetValue(ClientProperty); }
            set
            {
                if (this.Client != null)
                {
                    this.Client.ChannelsObserver.CollectionChanged -= this.ChannelsObserverOnCollectionChanged;
                }
                value.ChannelsObserver.CollectionChanged += this.ChannelsObserverOnCollectionChanged;

                this.SetValue(ClientProperty, value);
            }
        }
        #endregion

        #region Methods
        private static void OnClientChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var pivot = (RoomPivot)d;
            var oldClient = (IRCClient)e.OldValue;
            var newClient = (IRCClient)e.NewValue;

            if (oldClient != null)
            {
                oldClient.ChannelsObserver.CollectionChanged -= pivot.ChannelsObserverOnCollectionChanged;
            }

            newClient.ChannelsObserver.CollectionChanged += pivot.ChannelsObserverOnCollectionChanged;
            pivot.Rooms.ItemsSource = newClient.ChannelsObserver;
            pivot.Rooms.Title = newClient.Config.ServerConfig.Endpoint;
            pivot.Rooms.DataContext = newClient.Config;
        }

        private async void ChannelsObserverOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            switch (args.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var item in args.NewItems)
                    {
                        var room = item as IRCChannel;

                        if (room != null)
                        {
                            await this.SelectRoom(room);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                case NotifyCollectionChangedAction.Replace:
                case NotifyCollectionChangedAction.Reset:
                case NotifyCollectionChangedAction.Move:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void LeaveRoomClick(object sender, TappedRoutedEventArgs e)
        {
            if (this.Client != null)
            {
                var room = ((sender as Control)?.DataContext as IRCChannel);

                if (room != null)
                {
                    this.Client.Part(room);
                }
            }
        }

        private void LeaveServerTopClick(object sender, TappedRoutedEventArgs e)
        {
            var app = ((App)Application.Current);

            if (this.Client.Config != null && app.ConnectedClients.Any(c => c.Config == this.Client.Config))
            {
                var client = app.ConnectedClients.First(c => c.Config == this.Client.Config);
                client.Close();
                app.ConnectedClients.Remove(client);
            }

            this.Visibility = Visibility.Collapsed;
        }

        private void ListRightTapped(object sender, RightTappedRoutedEventArgs e)
        {
            FlyoutBase.ShowAttachedFlyout(sender as FrameworkElement);
        }

        private async Task SelectRoom(IRCChannel channel)
        {
            await this.Rooms.Dispatcher.RunAsync(CoreDispatcherPriority.High, () => this.Rooms.SelectedItem = channel);
        }
        #endregion

        private class CustomDataTemplateSelector : DataTemplateSelector
        {
            #region Public Properties
            public DataTemplate GlobalChannelTemplate { get; set; }
            public DataTemplate PrivateChannelTemplate { get; set; }
            public DataTemplate RawChannelTemplate { get; set; }
            #endregion

            #region Methods
            protected override DataTemplate SelectTemplateCore(object item)
            {
                if (item is IRCPrivateChannel)
                {
                    return this.PrivateChannelTemplate;
                }
                if (item is IRCGlobalChannel)
                {
                    return this.GlobalChannelTemplate;
                }

                return this.RawChannelTemplate;
            }

            protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
            {
                return this.SelectTemplateCore(item);
            }
            #endregion
        }
    }
}