﻿namespace OneIRC.IRC
{
    #region
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;

    #endregion

    public class IRCConnection : IDisposable
    {
        #region Constructors and Destructors
        public IRCConnection(IRCServerConfig config)
        {
            this.Config = config;
            this.ConnectionHandler += (sender, args) => Debug.WriteLine($"Connection {this.Config}: {args.Message}");
            this.MessageReceivedHandler +=
                (sender, args) => Debug.WriteLine($"Message Received {this.Config}: {args.Message}");
            this.MessageSentHandler += (sender, args) => Debug.WriteLine($"Message Sent {this.Config}: {args.Message}");
        }
        #endregion

        #region Public Events
        public event EventHandler<IRCEventArgs> ConnectionHandler;
        public event EventHandler<IRCEventArgs> MessageReceivedHandler;
        public event EventHandler<IRCEventArgs> MessageSentHandler;
        #endregion

        #region Public Properties
        public bool IsConnected => this.IRCSocket.Connected;
        #endregion

        #region Properties
        protected static int BufferSize => 512;

        protected IRCServerConfig Config { get; set; }
        protected string CurrentInput { get; set; }
        protected Socket IRCSocket { get; set; }
        #endregion

        #region Public Methods and Operators
        public void Close()
        {
            // Close the Sockets if they exist
            this.IRCSocket?.Dispose();
        }

        public void Dispose()
        {
            this.Close();
        }

        public void Open()
        {
            // Clear out any latent connections
            this.Close();

            // Set up the main IRC Socket
            this.IRCSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            var ircConnectArgs = new SocketAsyncEventArgs
                                     {
                                         RemoteEndPoint = new DnsEndPoint(this.Config.Endpoint, this.Config.Port)
                                     };
            ircConnectArgs.Completed += this.ConnectionMadeHandler;

            // Connect to the server
            if (!this.IRCSocket.ConnectAsync(ircConnectArgs))
            {
                this.ConnectionMadeHandler(this, ircConnectArgs);
            }
        }

        public bool SendMessage(string message, Guid id)
        {
            // Only call if the message exists
            if (string.IsNullOrWhiteSpace(message))
            {
                return false;
            }

            // Cleanup the message
            message = message.EndsWith("\r\n") ? message : $"{message}\r\n";

            // Set up the socket for sending
            var ircMessageArgs = new SocketAsyncEventArgs
                                     {
                                         RemoteEndPoint =
                                             new DnsEndPoint(this.Config.Endpoint, this.Config.Port),
                                         UserToken = id
                                     };
            var buffer = Encoding.ASCII.GetBytes(message);
            ircMessageArgs.SetBuffer(buffer, 0, buffer.Length);
            ircMessageArgs.Completed += this.SentHandler;

            Debug.WriteLine($"Sending Message: {message}");

            // Send
            if (this.IRCSocket.Connected && (!this.IRCSocket?.SendAsync(ircMessageArgs) ?? false))
            {
                this.SentHandler(null, ircMessageArgs);
            }
            else
            {
                return false;
            }

            return true;
        }
        #endregion

        #region Methods
        protected void ConnectionMadeHandler(object sender, SocketAsyncEventArgs args)
        {
            var connectionSuccess = (args.LastOperation == SocketAsyncOperation.Connect
                                     && args.SocketError == SocketError.Success);

            // If we connected, spin up the message queue
            if (connectionSuccess)
            {
                args.SetBuffer(new byte[BufferSize], 0, BufferSize);
                args.Completed -= this.ConnectionMadeHandler;
                args.Completed += this.ReceivedHandler;
                if (!this.IRCSocket?.ReceiveAsync(args) ?? false)
                {
                    this.ReceivedHandler(this, args);
                }
            }

            var notificationArgs = new IRCEventArgs
                                       {
                                           Status =
                                               connectionSuccess
                                                   ? IRCConnectionStatus.Accept_Complete
                                                   : IRCConnectionStatus.Accept_Error,
                                           Message = args.SocketError.ToString()
                                       };
            this.ConnectionHandler?.Invoke(this, notificationArgs);
        }

        protected void ParseMessages()
        {
            // Get all of the messages in the queue
            var msgs = this.CurrentInput.Split('\n');

            // For each one that ends in \r, notify people
            foreach (var msg in msgs.Where(msg => msg.EndsWith("\r")))
            {
                var notificationArgs = new IRCEventArgs
                                           {
                                               Status = IRCConnectionStatus.Message_Received,
                                               Message = msg.TrimEnd('\r')
                                           };

                this.MessageReceivedHandler?.Invoke(this, notificationArgs);
            }

            // If the last message didn't end in \r, keep it
            this.CurrentInput = "";
            if (!msgs[msgs.Length - 1].EndsWith("\r"))
            {
                this.CurrentInput = msgs[msgs.Length - 1];
            }
        }

        protected void ReceivedHandler(object sender, SocketAsyncEventArgs args)
        {
            // If there was no data recieved, the connection has been closed
            if (args.BytesTransferred == 0)
            {
                // Cleanup the connection
                this.Close();

                var notificationArgs = new IRCEventArgs
                                           {
                                               Status = IRCConnectionStatus.Socket_Closed,
                                               Message = "The server has gracefully closed this connection"
                                           };
                this.ConnectionHandler?.Invoke(this, notificationArgs);
            }
            else // Data was recieved
            {
                // Recieve the new data
                var tempBuffer = new byte[args.BytesTransferred];
                Array.Copy(args.Buffer, tempBuffer, args.BytesTransferred);
                this.CurrentInput += Encoding.ASCII.GetString(tempBuffer).Trim('\0');

                this.ParseMessages();

                // Ask for more data
                try
                {
                    args.SetBuffer(0, BufferSize);
                    if (!this.IRCSocket.ReceiveAsync(args))
                    {
                        this.ReceivedHandler(this, args);
                    }
                }
                catch (ObjectDisposedException)
                {
                    Debug.WriteLine("Socket has been closed");
                }
            }
        }

        protected void SentHandler(object sender, SocketAsyncEventArgs args)
        {
            // If there was no data sent, the connection has been closed
            if (args.BytesTransferred == 0)
            {
                // Cleanup the connection
                this.Close();

                var notificationArgs = new IRCEventArgs
                                           {
                                               Status = IRCConnectionStatus.Socket_Closed,
                                               Message = "The server has gracefully closed this connection"
                                           };
                this.ConnectionHandler?.Invoke(this, notificationArgs);
            }
            else // Data was sent
            {
                var sentSuccess = (args.LastOperation == SocketAsyncOperation.Send
                                   && args.SocketError == SocketError.Success);

                var notificationArgs = new IRCEventArgs((Guid)args.UserToken)
                                           {
                                               Status =
                                                   sentSuccess
                                                       ? IRCConnectionStatus
                                                             .Message_Sent
                                                       : IRCConnectionStatus
                                                             .Message_Send_Error,
                                               Message = args.SocketError.ToString()
                                           };

                this.MessageSentHandler?.Invoke(this, notificationArgs);
            }
        }
        #endregion
    }
}