﻿namespace OneIRC.IRC
{
    #region
    
    #endregion

    public class IRCServerConfig
    {
        #region Constructors and Destructors
        public IRCServerConfig(string endpoint, int port = 6667)
        {
            this.Endpoint = endpoint;
            this.Port = port;
        }
        #endregion

        #region Public Properties
        public string Endpoint { get; set; }
        public int Port { get; set; }
        #endregion
    }
}