﻿namespace OneIRC.IRC
{
    #region
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading;

    using OneIRC.IRC.Messages;

    #endregion

    public class IRCClient : IDisposable
    {
        #region Constructors and Destructors
        public IRCClient(IRCClientConfig config, SynchronizationContext context)
        {
            this.Config = config;

            this.RetryCount = 0;
            this.Nick = config.Nick;
            this.UIContext = context;

            this.Connection = new IRCConnection(config.ServerConfig);
            this.Connection.ConnectionHandler += this.ConnectionCallback;
            this.Connection.MessageReceivedHandler += this.MessageRecievedCallback;
            this.Connection.MessageSentHandler += this.MessageSentCallback;

            this.PendingSends = new Queue<IRCMessage>();
            this.PendingSends.Enqueue(new PassMessage(ConnectionPassword));
            this.PendingSends.Enqueue(new NickMessage(config.Nick));
            this.PendingSends.Enqueue(new UserMessage(config.Nick, 0, config.Username));

            this.OutstandingMessages = new Dictionary<Guid, IRCChannel>();
            this.Channels = new ConcurrentDictionary<string, IRCChannel>();
            this.Raw = new IRCChannel(this, "RAW");
            this.Global = new IRCGlobalChannel(this, "Global");

            this.ChannelsObserver = new ObservableCollection<IRCChannel>();
            this.AddChannelToList(this.Raw);
            this.AddChannelToList(this.Global);
        }
        #endregion

        #region Public Properties
        public ObservableCollection<IRCChannel> ChannelsObserver { get; }
        public IRCClientConfig Config { get; }

        public IRCChannel Global { get; }

        public bool IsConnected => this.Connection.IsConnected;

        public string Nick { get; protected set; }
        public IRCChannel Raw { get; }

        public SynchronizationContext UIContext { get; }
        #endregion

        #region Properties
        protected static string ConnectionPassword => "SuperSecretPassword";

        protected static int MaxRetries => 3;

        protected IDictionary<string, IRCChannel> Channels { get; }
        protected IRCConnection Connection { get; }

        protected IDictionary<Guid, IRCChannel> OutstandingMessages { get; }

        protected Queue<IRCMessage> PendingSends { get; }

        protected int RetryCount { get; set; }

        protected bool Sending { get; set; }
        #endregion

        #region Public Methods and Operators
        public void Close()
        {
            this.Connection.Close();
        }

        public void Dispose()
        {
            this.Close();
        }

        public void Join(string channel, string key)
        {
            channel = channel.Trim();

            if (!string.IsNullOrWhiteSpace(channel) && !this.Channels.ContainsKey(channel))
            {
                if (IRCMessage.IsValidChannel(channel))
                {
                    this.SendMessage(new JoinMessage(new[] { channel }, new[] { key }));
                }
                else
                {
                    this.Channels.Add(channel, new IRCPrivateChannel(this, channel));
                    this.AddChannelToList(this.Channels[channel]);
                }
            }
        }

        public void LeaveChannel(IRCChannel channel)
        {
            this.Channels.Remove(channel.Identifier);
            this.RemoveChannelFromList(channel);
        }

        public void Open()
        {
            this.Connection.Open();
        }

        public void Part(IRCChannel channel)
        {
            if (this.Channels.Values.Contains(channel))
            {
                if (channel is IRCPrivateChannel)
                {
                    this.SendMessage(new PartMessage(new[] { channel.Identifier }, ""));
                }
                else
                {
                    this.LeaveChannel(channel);
                }
            }
            else
            {
                this.RemoveChannelFromList(channel);
            }
        }

        public void SendMessage(IRCMessage message, IRCChannel channel = null)
        {
            lock (this.PendingSends)
            {
                if (message is ChannelMessage)
                {
                    this.HandleChannelMessages(message, true);
                }

                if (channel != null && message.Id != Guid.Empty)
                {
                    this.OutstandingMessages[message.Id] = channel;
                }
                else if (message.IsGlobalMessage)
                {
                    this.Global.HandleMessage(message);
                }

                this.PendingSends.Enqueue(message);

                this.AttemptSend();
            }
        }
        #endregion

        #region Methods
        protected void AddChannelToList(object channel)
        {
            var chan = channel as IRCChannel;

            if (chan != null)
            {
                if (SynchronizationContext.Current != this.UIContext)
                {
                    this.UIContext.Post(this.AddChannelToList, chan);
                }
                else
                {
                    this.ChannelsObserver.Add(chan);
                }
            }
        }

        protected void AttemptSend()
        {
            lock (this.PendingSends)
            {
                if (this.Sending || !this.PendingSends.Any())
                {
                    return;
                }

                var msg = this.PendingSends.Peek();
                this.Raw.HandleMessage(msg);
                this.Sending = true;
                this.Connection.SendMessage(msg.RawMessage, msg.Id);
            }
        }

        protected void ConnectionCallback(object sender, IRCEventArgs args)
        {
            if (args.Status == IRCConnectionStatus.Accept_Error)
            {
                this.RetryCount++;

                if (this.RetryCount < MaxRetries)
                {
                    this.Connection.Close();
                    this.Connection.Open();
                }
            }
            else // Successfully connected
            {
                this.RetryCount = 0;

                this.AttemptSend();
            }
        }

        protected void HandleChannelMessages(IRCMessage message, bool isSending)
        {
            var msg = message as ChannelMessage;

            if (msg != null)
            {
                foreach (var target in
                    msg.Targets.Where(t => !this.Nick.Equals(t, StringComparison.CurrentCultureIgnoreCase)))
                {
                    if (!this.Channels.ContainsKey(target))
                    {
                        this.Channels.Add(target, new IRCPrivateChannel(this, target));
                        this.AddChannelToList(this.Channels[target]);
                    }

                    this.Channels[target].HandleMessage(message);
                }
            }
        }

        protected void MessageRecievedCallback(object sender, IRCEventArgs args)
        {
            var message = IRCMessage.Parse(args.Message);
            this.Raw.HandleMessage(message);

            if (message is ChannelMessage)
            {
                this.HandleChannelMessages(message, false);
            }
            else
            {
                if (message is PingMessage)
                {
                    var msg = message as PingMessage;
                    this.SendMessage(new PongMessage(msg.Message));
                }
                else if (message.IsGlobalMessage)
                {
                    this.Global.HandleMessage(message);
                }
            }
        }

        protected void MessageSentCallback(object sender, IRCEventArgs args)
        {
            lock (this.PendingSends)
            {
                if (args.Status == IRCConnectionStatus.Message_Sent)
                {
                    var msg = this.PendingSends.Dequeue();

                    if (this.OutstandingMessages.ContainsKey(msg.Id))
                    {
                        this.OutstandingMessages[msg.Id].MessageSent(msg.Id);
                        this.OutstandingMessages.Remove(msg.Id);
                    }
                }

                this.Sending = false;
                this.AttemptSend();
            }
        }

        protected void RemoveChannelFromList(object channel)
        {
            var chan = channel as IRCChannel;

            if (chan != null)
            {
                if (SynchronizationContext.Current != this.UIContext)
                {
                    this.UIContext.Post(this.RemoveChannelFromList, chan);
                }
                else
                {
                    this.ChannelsObserver.Remove(chan);
                }
            }
        }
        #endregion
    }
}