﻿namespace OneIRC.IRC
{
    public enum IRCConnectionStatus
    {
        Accept_Complete,

        Accept_Error,

        Socket_Closed,

        Message_Received,

        Message_Sent,

        Message_Send_Error
    }
}