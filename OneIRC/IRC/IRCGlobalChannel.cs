﻿namespace OneIRC.IRC
{
    public class IRCGlobalChannel : IRCChannel
    {
        #region Constructors and Destructors
        public IRCGlobalChannel(IRCClient parent, string identifier) : base(parent, identifier) {}
        #endregion
    }
}