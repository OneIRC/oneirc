﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;
    using System.Collections.Generic;

    #endregion

    public class WhoWasMessage : IRCMessage
    {
        #region Constructors and Destructors
        public WhoWasMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public WhoWasMessage(IEnumerable<string> nicks, int count, string target, Guid id = new Guid()) : base(null, id)
        {
            this.Nicks = nicks;
            this.Count = count;
            this.Target = target;
        }

        public WhoWasMessage(IEnumerable<string> nicks, int count, Guid id = new Guid()) : this(nicks, count, "", id) {}

        public WhoWasMessage(IEnumerable<string> nicks, Guid id = new Guid()) : this(nicks, -1, "", id) {}

        public WhoWasMessage(string nick, int count, string target, Guid id = new Guid())
            : this(new[] { nick }, count, target, id) {}

        public WhoWasMessage(string nick, int count, Guid id = new Guid()) : this(new[] { nick }, count, id) {}

        public WhoWasMessage(string nick, Guid id = new Guid()) : this(new[] { nick }, id) {}
        #endregion

        #region Public Properties
        public int Count { get; }

        public override bool IsGlobalMessage => true;
        public IEnumerable<string> Nicks { get; }

        public override string RawMessage { get { throw new NotImplementedException(); } }

        public string Target { get; }
        #endregion
    }
}