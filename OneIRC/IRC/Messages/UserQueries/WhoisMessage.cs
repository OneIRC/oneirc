﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;
    using System.Collections.Generic;

    #endregion

    public class WhoisMessage : IRCMessage
    {
        #region Constructors and Destructors
        public WhoisMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public WhoisMessage(IEnumerable<string> masks, string target, Guid id = new Guid()) : base(null, id)
        {
            this.Masks = masks;
            this.Target = target;
        }

        public WhoisMessage(IEnumerable<string> masks, Guid id = new Guid()) : this(masks, "", id) {}

        public WhoisMessage(string mask, string target, Guid id = new Guid()) : this(new[] { mask }, target, id) {}

        public WhoisMessage(string mask, Guid id = new Guid()) : this(new[] { mask }, id) {}
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;
        public IEnumerable<string> Masks { get; }

        public override string RawMessage { get { throw new NotImplementedException(); } }

        public string Target { get; }
        #endregion
    }
}