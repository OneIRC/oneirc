﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class WhoMessage : IRCMessage
    {
        #region Constructors and Destructors
        public WhoMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public WhoMessage(string mask, bool opsOnly, Guid id = new Guid()) : base(null, id)
        {
            this.Mask = mask;
            this.OperatorsOnly = opsOnly;
        }
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;
        public string Mask { get; }
        public bool OperatorsOnly { get; }

        public override string RawMessage { get { throw new NotImplementedException(); } }
        #endregion
    }
}