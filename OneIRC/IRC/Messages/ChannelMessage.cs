﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;
    using System.Collections.Generic;

    #endregion

    public abstract class ChannelMessage : IRCMessage
    {
        #region Constructors and Destructors
        protected ChannelMessage(string prefix, Guid id = new Guid()) : base(prefix, id) {}
        #endregion

        #region Public Properties
        public abstract IEnumerable<string> Targets { get; }
        #endregion
    }
}