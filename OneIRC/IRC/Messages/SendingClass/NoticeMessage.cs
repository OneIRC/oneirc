﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class NoticeMessage : SendingMessage
    {
        #region Constructors and Destructors
        public NoticeMessage(string args, string prefix) : base(args, prefix) {}

        public NoticeMessage(string target, string message, Guid id = new Guid()) : base(target, message, id) {}
        #endregion

        #region Public Properties
        public override string RawMessage { get { throw new NotImplementedException(); } }
        #endregion
    }
}