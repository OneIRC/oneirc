﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;
    using System.Collections.Generic;

    #endregion

    public abstract class SendingMessage : ChannelMessage
    {
        #region Constructors and Destructors
        protected SendingMessage(string args, string prefix) : base(prefix)
        {
            var splitArgs = args.Split(":".ToCharArray(), 2);
            this.Message = splitArgs[1];
            this.Target = splitArgs[0].TrimEnd();
        }

        protected SendingMessage(string target, string message, Guid id) : base(null, id)
        {
            this.Message = message;
            this.Target = target;
        }
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => false;
        public string Message { get; }

        public string Target { get; }

        public override IEnumerable<string> Targets
        {
            get
            {
                // If the message is a direct message
                if (char.IsLetter(this.Target[0]) && !string.IsNullOrEmpty(this.NickName))
                {
                    return new[] { this.Target, this.NickName };
                }

                return new[] { this.Target };
            }
        }
        #endregion
    }
}