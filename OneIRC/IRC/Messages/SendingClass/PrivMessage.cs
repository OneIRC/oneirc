﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class PrivMessage : SendingMessage
    {
        #region Constructors and Destructors
        public PrivMessage(string args, string prefix) : base(args, prefix) {}

        public PrivMessage(string target, string message, Guid id = new Guid()) : base(target, message, id) {}
        #endregion

        #region Public Properties
        public override string RawMessage => $"PRIVMSG {this.Target} :{this.Message}";
        #endregion
    }
}