﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class AwayMessage : IRCMessage
    {
        #region Constructors and Destructors
        public AwayMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public AwayMessage(string message, Guid id = new Guid()) : base(null, id)
        {
            this.Message = message;
        }

        public AwayMessage(Guid id = new Guid()) : this("", id) {}
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;
        public string Message { get; }

        public override string RawMessage { get { throw new NotImplementedException(); } }
        #endregion
    }
}