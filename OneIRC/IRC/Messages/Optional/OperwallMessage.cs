﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class OperwallMessage : IRCMessage
    {
        #region Constructors and Destructors
        public OperwallMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public OperwallMessage(string message, Guid id = new Guid()) : base(null, id)
        {
            this.Message = message;
        }
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;
        public string Message { get; }
        public override string RawMessage => $"WALLOPS {this.Message}";
        #endregion
    }
}