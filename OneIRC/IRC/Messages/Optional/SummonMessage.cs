﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class SummonMessage : IRCMessage
    {
        #region Constructors and Destructors
        public SummonMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public SummonMessage(string user, string target, string channel, Guid id = new Guid()) : base(null, id)
        {
            this.User = user;
            this.TargetServer = target;
            this.Channel = channel;
        }

        public SummonMessage(string user, string target, Guid id = new Guid()) : this(user, target, "", id) {}

        public SummonMessage(string user, Guid id = new Guid()) : this(user, "", "", id) {}
        #endregion

        #region Public Properties
        public string Channel { get; }

        public override bool IsGlobalMessage => true;

        public override string RawMessage { get { throw new NotImplementedException(); } }

        public string TargetServer { get; }
        public string User { get; }
        #endregion
    }
}