﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class DieMessage : IRCMessage
    {
        #region Constructors and Destructors
        public DieMessage(string prefix) : base(prefix) {}

        public DieMessage(Guid id = new Guid()) : base(null, id) {}
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;
        public override string RawMessage => "DIE";
        #endregion
    }
}