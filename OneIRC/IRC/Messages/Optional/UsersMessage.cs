﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class UsersMessage : IRCMessage
    {
        #region Constructors and Destructors
        public UsersMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public UsersMessage(string server, Guid id = new Guid()) : base(null, id)
        {
            this.TargetServer = server;
        }

        public UsersMessage(Guid id = new Guid()) : this("", id) {}
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;

        public override string RawMessage { get { throw new NotImplementedException(); } }

        public string TargetServer { get; }
        #endregion
    }
}