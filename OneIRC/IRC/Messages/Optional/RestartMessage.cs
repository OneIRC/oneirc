﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class RestartMessage : IRCMessage
    {
        #region Constructors and Destructors
        public RestartMessage(string prefix) : base(prefix) {}

        public RestartMessage(Guid id = new Guid()) : base(null, id) {}
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;
        public override string RawMessage => "RESTART";
        #endregion
    }
}