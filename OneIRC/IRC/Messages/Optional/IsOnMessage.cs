﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;
    using System.Collections.Generic;

    #endregion

    public class IsOnMessage : IRCMessage
    {
        #region Constructors and Destructors
        public IsOnMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public IsOnMessage(IEnumerable<string> nicks, Guid id = new Guid()) : base(null, id)
        {
            this.TargetNicknames = nicks;
        }

        public IsOnMessage(string nick, Guid id = new Guid()) : this(new[] { nick }, id) {}
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;
        public override string RawMessage => $"ISON {string.Join(" ", this.TargetNicknames)}";
        public IEnumerable<string> TargetNicknames { get; }
        #endregion
    }
}