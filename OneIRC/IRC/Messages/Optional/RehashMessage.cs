﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class RehashMessage : IRCMessage
    {
        #region Constructors and Destructors
        public RehashMessage(string prefix) : base(prefix) {}

        public RehashMessage(Guid id = new Guid()) : base(null, id) {}
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;
        public override string RawMessage => "REHASH";
        #endregion
    }
}