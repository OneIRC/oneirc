﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;
    using System.Collections.Generic;

    #endregion

    public class UserhostMessage : IRCMessage
    {
        #region Constructors and Destructors
        public UserhostMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public UserhostMessage(IEnumerable<string> nicks, Guid id = new Guid()) : base(null, id)
        {
            this.TargetNicknames = nicks;
        }

        public UserhostMessage(string nick, Guid id = new Guid()) : this(new[] { nick }, id) {}
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;
        public override string RawMessage => $"USERHOST {string.Join(" ", this.TargetNicknames)}";
        public IEnumerable<string> TargetNicknames { get; }
        #endregion
    }
}