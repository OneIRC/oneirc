﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class TopicMessage : IRCMessage
    {
        #region Constructors and Destructors
        public TopicMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public TopicMessage(string channel, string topic, Guid id = new Guid()) : base(null, id)
        {
            this.Channel = channel;
            this.Topic = topic;
            this.IsAssignment = false;
        }

        public TopicMessage(string channel, Guid id = new Guid()) : base(null, id)
        {
            this.Channel = channel;
            this.Topic = "";
            this.IsAssignment = true;
        }
        #endregion

        #region Public Properties
        public string Channel { get; }
        public bool IsAssignment { get; }

        public override bool IsGlobalMessage => false;

        public override string RawMessage { get { throw new NotImplementedException(); } }

        public string Topic { get; }
        #endregion
    }
}