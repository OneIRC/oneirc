﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;
    using System.Collections.Generic;

    #endregion

    public class ListMessage : ChannelMessage
    {
        #region Constructors and Destructors
        public ListMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public ListMessage(IEnumerable<string> channels, string targetServer, Guid id = new Guid()) : base(null, id)
        {
            this.Channels = channels;
            this.TargetServer = targetServer;
        }

        public ListMessage(IEnumerable<string> channels, Guid id = new Guid()) : this(channels, "", id) {}

        public ListMessage(string channel, Guid id = new Guid()) : base(null, id)
        {
            this.Channels = new[] { channel };
            this.TargetServer = "";
        }

        public ListMessage(Guid id = new Guid()) : base(null, id)
        {
            this.Channels = new string[0];
            this.TargetServer = "";
        }
        #endregion

        #region Public Properties
        public IEnumerable<string> Channels { get; }

        public override bool IsGlobalMessage => false;

        public override string RawMessage { get { throw new NotImplementedException(); } }

        public override IEnumerable<string> Targets => this.Channels;
        public string TargetServer { get; }
        #endregion
    }
}