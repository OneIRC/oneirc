﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;
    using System.Collections.Generic;

    #endregion

    public class NamesMessage : IRCMessage
    {
        #region Constructors and Destructors
        public NamesMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public NamesMessage(IEnumerable<string> channels, string target, Guid id = new Guid()) : base(null, id)
        {
            this.Channels = channels;
            this.Target = target;
        }

        public NamesMessage(IEnumerable<string> channels, Guid id = new Guid()) : base(null, id)
        {
            this.Channels = channels;
            this.Target = "";
        }

        public NamesMessage(string channel, Guid id = new Guid()) : base(null, id)
        {
            this.Channels = new[] { channel };
            this.Target = "";
        }

        public NamesMessage(Guid id = new Guid()) : base(null, id)
        {
            this.Channels = new string[0];
            this.Target = "";
        }
        #endregion

        #region Public Properties
        public IEnumerable<string> Channels { get; }

        public override bool IsGlobalMessage => false;

        public override string RawMessage { get { throw new NotImplementedException(); } }

        public string Target { get; }
        #endregion
    }
}