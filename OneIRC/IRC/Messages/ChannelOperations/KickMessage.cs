﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;
    using System.Collections.Generic;

    #endregion

    public class KickMessage : ChannelMessage
    {
        #region Constructors and Destructors
        public KickMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public KickMessage(
            IEnumerable<string> channels,
            IEnumerable<string> nicks,
            string comment,
            Guid id = new Guid()) : base(null, id)
        {
            this.Channels = channels;
            this.TargetNickNames = nicks;
            this.Comment = comment;
        }

        public KickMessage(IEnumerable<string> channels, IEnumerable<string> nicks, Guid id = new Guid())
            : this(channels, nicks, "", id) {}

        public KickMessage(string channel, string nick, string comment, Guid id = new Guid()) : base(null, id)
        {
            this.Channels = new[] { channel };
            this.TargetNickNames = new[] { nick };
            this.Comment = comment;
        }

        public KickMessage(string channel, string nick, Guid id = new Guid()) : this(channel, nick, "", id) {}
        #endregion

        #region Public Properties
        public IEnumerable<string> Channels { get; }
        public string Comment { get; }

        public override bool IsGlobalMessage => true;

        public override string RawMessage { get { throw new NotImplementedException(); } }

        public IEnumerable<string> TargetNickNames { get; }
        public override IEnumerable<string> Targets => this.Channels;
        #endregion
    }
}