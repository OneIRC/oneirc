﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;
    using System.Collections.Generic;

    #endregion

    public class ChannelModeMessage : ChannelMessage
    {
        #region Constructors and Destructors
        public ChannelModeMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public ChannelModeMessage(string channel, IEnumerable<ChannelMode> modes, Guid id = new Guid()) : base(null, id)
        {
            this.Channel = channel;
            this.Modes = modes;
        }
        #endregion

        #region Public Properties
        public string Channel { get; }

        public override bool IsGlobalMessage => false;
        public IEnumerable<ChannelMode> Modes { get; }

        public override string RawMessage { get { throw new NotImplementedException(); } }

        public override IEnumerable<string> Targets => new[] { this.Channel };
        #endregion
    }

    public enum ChannelModeFlags
    {
        Moderated,

        InviteOnly,

        AutoInvite,

        ChanOp,

        Voice,

        Secret,

        Key,

        UserLimit,

        BanMask,

        BanExceptMask,

        ExceptionMask,

        InvitationMask,

        Creator
    }

    public enum ChannelModeChangeType
    {
        Add,

        Remove,

        None
    }

    public abstract class ChannelMode
    {
        #region Constructors and Destructors
        public ChannelMode(ChannelModeChangeType type)
        {
            this.Type = type;
        }
        #endregion

        #region Public Properties
        public abstract string RawMode { get; }
        public abstract bool RequireParam { get; }
        public ChannelModeChangeType Type { get; protected set; }
        #endregion
    }

    public class ModeratedChannelMode : ChannelMode
    {
        #region Constructors and Destructors
        public ModeratedChannelMode(ChannelModeChangeType type) : base(type) {}
        #endregion

        #region Public Properties
        public override string RawMode => "m";
        public override bool RequireParam => false;
        #endregion
    }

    public class InviteOnlyChannelMode : ChannelMode
    {
        #region Constructors and Destructors
        public InviteOnlyChannelMode(ChannelModeChangeType type) : base(type) {}
        #endregion

        #region Public Properties
        public override string RawMode => "i";
        public override bool RequireParam => false;
        #endregion
    }

    public class AutoInviteChannelMode : ChannelMode
    {
        #region Constructors and Destructors
        public AutoInviteChannelMode(string mask, ChannelModeChangeType type) : base(type)
        {
            this.Mask = mask;
        }
        #endregion

        #region Public Properties
        public string Mask { get; }
        public override string RawMode => $"I {this.Mask}";
        public override bool RequireParam => true;
        #endregion
    }

    public class ChanOpChannelMode : ChannelMode
    {
        #region Constructors and Destructors
        public ChanOpChannelMode(string nick, ChannelModeChangeType type) : base(type)
        {
            this.Nick = nick;
        }
        #endregion

        #region Public Properties
        public string Nick { get; }
        public override string RawMode => $"o {this.Nick}";
        public override bool RequireParam => true;
        #endregion
    }

    public class VoiceChannelMode : ChannelMode
    {
        #region Constructors and Destructors
        public VoiceChannelMode(string nick, ChannelModeChangeType type) : base(type)
        {
            this.Nick = nick;
        }
        #endregion

        #region Public Properties
        public string Nick { get; }
        public override string RawMode => $"v {this.Nick}";
        public override bool RequireParam => true;
        #endregion
    }

    public class SecretChannelMode : ChannelMode
    {
        #region Constructors and Destructors
        public SecretChannelMode(ChannelModeChangeType type) : base(type) {}
        #endregion

        #region Public Properties
        public override string RawMode => "s";
        public override bool RequireParam => false;
        #endregion
    }

    public class KeyChannelMode : ChannelMode
    {
        #region Constructors and Destructors
        public KeyChannelMode(string key, ChannelModeChangeType type) : base(type)
        {
            this.Key = key;
        }
        #endregion

        #region Public Properties
        public string Key { get; }
        public override string RawMode => $"k {this.Key}";
        public override bool RequireParam => true;
        #endregion
    }

    public class UserLimitChannelMode : ChannelMode
    {
        #region Constructors and Destructors
        public UserLimitChannelMode(string limit, ChannelModeChangeType type) : base(type)
        {
            this.Limit = int.Parse(limit);
        }
        #endregion

        #region Public Properties
        public int Limit { get; }
        public override string RawMode => $"l {this.Limit}";
        public override bool RequireParam => true;
        #endregion
    }

    public class BanMaskChannelMode : ChannelMode
    {
        #region Constructors and Destructors
        public BanMaskChannelMode(string mask, ChannelModeChangeType type) : base(type)
        {
            this.Mask = mask;
        }
        #endregion

        #region Public Properties
        public string Mask { get; }

        public override string RawMode
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.Mask))
                {
                    return "b";
                }
                return $"b {this.Mask}";
            }
        }

        public override bool RequireParam => true;
        #endregion
    }

    public class ExceptionChannelMode : ChannelMode
    {
        #region Constructors and Destructors
        public ExceptionChannelMode(string mask, ChannelModeChangeType type) : base(type)
        {
            this.Mask = mask;
        }
        #endregion

        #region Public Properties
        public string Mask { get; }

        public override string RawMode
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.Mask))
                {
                    return "e";
                }
                return $"e {this.Mask}";
            }
        }

        public override bool RequireParam => true;
        #endregion
    }

    public class CreatorChannelMode : ChannelMode
    {
        #region Constructors and Destructors
        public CreatorChannelMode(ChannelModeChangeType type) : base(type) {}
        #endregion

        #region Public Properties
        public override string RawMode => "O";
        public override bool RequireParam => false;
        #endregion
    }
}