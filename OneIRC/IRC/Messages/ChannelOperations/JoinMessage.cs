﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;
    using System.Collections.Generic;

    #endregion

    public class JoinMessage : ChannelMessage
    {
        #region Constructors and Destructors
        public JoinMessage(string args, string prefix) : base(prefix)
        {
            var strings = args.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            this.Channels = strings[0].Split(",".ToCharArray());

            this.Keys = new List<string>();
            if (strings.Length == 2)
            {
                this.Keys = strings[1].Split(",".ToCharArray());
            }
        }

        public JoinMessage(IEnumerable<string> channels, IEnumerable<string> keys, Guid id = new Guid())
            : base(null, id)
        {
            this.Channels = channels;
            this.Keys = keys;
        }
        #endregion

        #region Public Properties
        public IEnumerable<string> Channels { get; }

        public override bool IsGlobalMessage => true;
        public IEnumerable<string> Keys { get; }
        public override string RawMessage => $"JOIN {string.Join(",", this.Channels)} {string.Join(",", this.Keys)}";
        public override IEnumerable<string> Targets => this.Channels;
        #endregion
    }
}