﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;
    using System.Collections.Generic;

    #endregion

    public class InviteMessage : ChannelMessage
    {
        #region Constructors and Destructors
        public InviteMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public InviteMessage(string targetNick, string channel, Guid id = new Guid()) : base(null, id)
        {
            this.TargetNickName = targetNick;
            this.Channel = channel;
        }
        #endregion

        #region Public Properties
        public string Channel { get; }

        public override bool IsGlobalMessage => false;

        public override string RawMessage { get { throw new NotImplementedException(); } }

        public string TargetNickName { get; }
        public override IEnumerable<string> Targets => new[] { this.Channel };
        #endregion
    }
}