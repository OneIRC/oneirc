﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;
    using System.Collections.Generic;

    #endregion

    public class PartMessage : ChannelMessage
    {
        #region Constructors and Destructors
        public PartMessage(string args, string prefix) : base(prefix)
        {
            var splitArgs = args.Split(":".ToCharArray(), 2);
            this.Message = splitArgs.Length == 2 ? splitArgs[1] : "";
            this.Channels = splitArgs[0].Split(", ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        }

        public PartMessage(IEnumerable<string> channels, string message, Guid id = new Guid()) : base(null, id)
        {
            this.Message = message;
            this.Channels = channels;
        }
        #endregion

        #region Public Properties
        public IEnumerable<string> Channels { get; }

        public override bool IsGlobalMessage => false;
        public string Message { get; }
        public override string RawMessage => $"PART {string.Join(" ", this.Channels)} :{this.Message}";
        public override IEnumerable<string> Targets => this.Channels;
        #endregion
    }
}