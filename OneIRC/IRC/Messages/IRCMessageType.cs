﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    using OneIRC.IRC.Messages.Numbers;

    #endregion

    public enum IRCMessageType
    {
        Nick,
        User,
        Ping,
        Pong,
        Join,
        Part,
        Privmsg,
        Quit,
        Number,
        Pass,
        Unknown
    }

    public static class IRCMessageTypeExtensions
    {
        #region Public Methods and Operators
        public static IRCMessage Parse(this IRCMessageType type, string prefix, string args, int num = -1)
        {
            switch (type)
            {
                case IRCMessageType.Nick:
                    return new NickMessage(args, prefix);
                case IRCMessageType.User:
                    return new UserMessage(args, prefix);
                case IRCMessageType.Ping:
                    return new PingMessage(args, prefix);
                case IRCMessageType.Pong:
                    return new PongMessage(args, prefix);
                case IRCMessageType.Join:
                    return new JoinMessage(args, prefix);
                case IRCMessageType.Part:
                    return new PartMessage(args, prefix);
                case IRCMessageType.Privmsg:
                    return new PrivMessage(args, prefix);
                case IRCMessageType.Quit:
                    return new QuitMessage(args, prefix);
                case IRCMessageType.Number:
                    return NumberMessage.Parse(num, args, prefix);
                case IRCMessageType.Pass:
                    return new PassMessage(args, prefix);
                case IRCMessageType.Unknown:
                    return NumberMessage.Parse(num, args, prefix);
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
        #endregion
    }
}