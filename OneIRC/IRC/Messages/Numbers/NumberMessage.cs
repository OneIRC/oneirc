﻿namespace OneIRC.IRC.Messages.Numbers
{
    public class NumberMessage : IRCMessage
    {
        #region Constructors and Destructors
        protected NumberMessage(int number, string args, string prefix) : base(prefix)
        {
            this.Number = number;
            this.Message = args;
        }
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;
        public string Message { get; }
        public int Number { get; }
        public override string RawMessage => $"{this.Number} {this.Message}";
        #endregion

        #region Public Methods and Operators
        public static IRCMessage Parse(int number, string args, string prefix)
        {
            switch (number)
            {
                case 353:
                    return new Num353Message(args, prefix);
                case 366:
                    return new Num366Message(args, prefix);
                default:
                    return new NumberMessage(number, args, prefix);
            }
        }
        #endregion
    }
}