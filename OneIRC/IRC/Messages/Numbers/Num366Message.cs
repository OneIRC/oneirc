﻿namespace OneIRC.IRC.Messages.Numbers
{
    #region
    using System;
    using System.Collections.Generic;
    using System.Linq;

    #endregion

    public class Num366Message : ChannelMessage
    {
        #region Constructors and Destructors
        public Num366Message(string args, string prefix) : base(prefix)
        {
            var argSplit = args.Split(" ".ToCharArray());

            this.TargetNickName = argSplit[0].Trim();
            this.Targets = new[] { argSplit[1].Trim() };
        }

        public Num366Message(string targetNick, string targetChan, Guid id) : base(null, id)
        {
            this.TargetNickName = targetNick;
            this.Targets = new[] { targetChan };
        }
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => false;
        public override string RawMessage => $"366 {this.TargetNickName} {this.Targets.First()} :End of /NAMES list.";
        public string TargetNickName { get; }
        public override IEnumerable<string> Targets { get; }
        #endregion
    }
}