﻿namespace OneIRC.IRC.Messages.Numbers
{
    #region
    using System;
    using System.Collections.Generic;
    using System.Linq;

    #endregion

    public class Num353Message : ChannelMessage
    {
        #region Constructors and Destructors
        public Num353Message(string args, string prefix) : base(prefix)
        {
            var argSplit = args.Split(":".ToCharArray());
            var infoArgs = argSplit[0].Split(" ".ToCharArray());

            this.TargetNickName = infoArgs[0];

            switch (infoArgs[1])
            {
                case "@":
                    this.ChannelType = ChannelType.Secret;
                    break;
                case "*":
                    this.ChannelType = ChannelType.Private;
                    break;
                case "=":
                default:
                    this.ChannelType = ChannelType.Public;
                    break;
            }

            this.Targets = new[] { infoArgs[2] };
            this.Nicks = argSplit[1].Split(" ".ToCharArray());
        }

        public Num353Message(string targetNick, ChannelType type, string targetChan, IEnumerable<string> nicks, Guid id)
            : base(null, id)
        {
            this.TargetNickName = targetNick;
            this.ChannelType = type;
            this.Targets = new[] { targetChan };
            this.Nicks = nicks;
        }
        #endregion

        #region Public Properties
        public ChannelType ChannelType { get; }

        public override bool IsGlobalMessage => false;
        public IEnumerable<string> Nicks { get; }

        public override string RawMessage
        {
            get
            {
                string msgType;
                switch (this.ChannelType)
                {
                    case ChannelType.Secret:
                        msgType = "@";
                        break;
                    case ChannelType.Private:
                        msgType = "*";
                        break;
                    case ChannelType.Public:
                    default:
                        msgType = "=";
                        break;
                }

                return $"353 {this.TargetNickName} {msgType} {this.Targets.First()} :{string.Join(" ", this.Nicks)}";
            }
        }

        public string TargetNickName { get; }
        public override IEnumerable<string> Targets { get; }
        #endregion
    }

    public enum ChannelType
    {
        Secret,

        Private,

        Public
    }
}