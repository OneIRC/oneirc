﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;
    using System.Text.RegularExpressions;

    #endregion

    public abstract class IRCMessage
    {
        #region Constructors and Destructors
        protected IRCMessage(string prefix, Guid id = new Guid())
        {
            this.ParsePrefix(prefix);

            this.Id = id;
            this.Time = DateTime.Now;
        }
        #endregion

        #region Public Properties
        public string CompleteMessage => $":{this.NickName}!{this.UserName}@{this.ServerName} {this.RawMessage}";
        public string HostName { get; private set; }
        public Guid Id { get; private set; }

        public abstract bool IsGlobalMessage { get; }
        public string NickName { get; set; }
        public abstract string RawMessage { get; }
        public string ServerName { get; private set; }
        public DateTime Time { get; private set; }
        public string UserName { get; private set; }
        #endregion

        #region Parse
        public static IRCMessage Parse(string message)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentException("Message invalid", nameof(message));
            }

            // Clean the input
            message = message.Trim();

            // Switch between the two message types
            if (message.StartsWith(":"))
            {
                var msgParts = message.Split(" ".ToCharArray(), 3);

                if (msgParts.Length == 3)
                {
                    return Parse3Part(message, msgParts[1], msgParts[2], msgParts[0]);
                }
            }
            else
            {
                var msgParts = message.Split(" ".ToCharArray(), 2);

                if (msgParts.Length == 2)
                {
                    return Parse3Part(message, msgParts[0], msgParts[1]);
                }
            }

            // Error
            throw new ArgumentException("Message invalid", nameof(message));
        }

        protected static IRCMessage Parse3Part(string message, string command, string args, string prefix = null)
        {
            // Parse command
            int number;
            var cmd = ParseCommand(command, out number);

            // Allow the sub-class to parse itself
            return cmd.Parse(prefix, args, number);
        }

        protected void ParsePrefix(string prefix)
        {
            if (string.IsNullOrWhiteSpace(prefix))
            {
                return;
            }

            var prefixParts = prefix.Split("!@".ToCharArray());

            switch (prefixParts.Length)
            {
                case 1:
                    this.ServerName = prefix;
                    break;
                case 2:
                    this.HostName = prefixParts[1];
                    this.NickName = prefixParts[0].Trim(":".ToCharArray());
                    break;
                case 3:
                    this.HostName = prefixParts[2];
                    this.NickName = prefixParts[0].Trim(":".ToCharArray());
                    this.UserName = prefixParts[1];
                    break;
                default:
                    throw new ArgumentException("Message invalid", nameof(prefix));
            }
        }

        protected static IRCMessageType ParseCommand(string command, out int number)
        {
            var cmd = IRCMessageType.Unknown;
            number = -1;

            if (Regex.IsMatch(command, "^[0-9]+$"))
            {
                cmd = IRCMessageType.Number;

                number = int.Parse(command);
            }
            else
            {
                try
                {
                    cmd = (IRCMessageType)Enum.Parse(typeof(IRCMessageType), command, true);
                }
                catch (ArgumentException)
                {
                    // TODO: Log Error
                }
            }

            return cmd;
        }

        public static bool IsValidChannel(string name)
        {
            return name.StartsWith("#") || name.StartsWith("&") || name.StartsWith("!");
        }
        #endregion
    }
}