﻿namespace OneIRC.IRC.Messages
{
    using System;

    public class MotdMessage : IRCMessage
    {
        public string TargetServer { get; }

        public MotdMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public MotdMessage(string target, Guid id = new Guid()) : base(null, id)
        {
            this.TargetServer = target;
        }

        public MotdMessage(Guid id = new Guid()) : this("", id) { }


        public override bool ChannelMessage => false;
        public override string RawMessage
        {
            get
            {
                throw new NotImplementedException();
            }
        }
    }
}
