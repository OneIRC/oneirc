﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class ErrorMessage : IRCMessage
    {
        #region Constructors and Destructors
        public ErrorMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public ErrorMessage(string message, Guid id = new Guid()) : base(null, id)
        {
            this.Message = message;
        }
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;
        public string Message { get; }

        public override string RawMessage { get { throw new NotImplementedException(); } }
        #endregion
    }
}