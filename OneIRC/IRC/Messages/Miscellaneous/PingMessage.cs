﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class PingMessage : IRCMessage
    {
        #region Constructors and Destructors
        public PingMessage(string args, string prefix) : base(prefix)
        {
            this.Message = args.Substring(1);
        }

        public PingMessage(string message, Guid id = new Guid()) : base(null, id)
        {
            this.Message = message;
        }
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => false;
        public string Message { get; }
        public override string RawMessage => $"PING :{this.Message}";
        #endregion
    }
}