﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class KillMessage : IRCMessage
    {
        #region Constructors and Destructors
        public KillMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public KillMessage(string nick, string comment, Guid id = new Guid()) : base(null, id)
        {
            this.TargetNickName = nick;
            this.Comment = comment;
        }
        #endregion

        #region Public Properties
        public string Comment { get; }

        public override bool IsGlobalMessage => true;

        public override string RawMessage { get { throw new NotImplementedException(); } }

        public string TargetNickName { get; }
        #endregion
    }
}