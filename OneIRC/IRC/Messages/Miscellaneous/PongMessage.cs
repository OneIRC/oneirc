﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class PongMessage : IRCMessage
    {
        #region Constructors and Destructors
        public PongMessage(string args, string prefix) : base(prefix)
        {
            this.Message = args.Substring(1);
        }

        public PongMessage(string message, Guid id = new Guid()) : base(null, id)
        {
            this.Message = message;
        }
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => false;
        public string Message { get; }
        public override string RawMessage => $"PONG :{this.Message}";
        #endregion
    }
}