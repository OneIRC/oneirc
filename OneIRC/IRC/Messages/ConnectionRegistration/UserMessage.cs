﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class UserMessage : IRCMessage
    {
        #region Constructors and Destructors
        public UserMessage(string args, string prefix = null) : base(prefix)
        {
            if (string.IsNullOrWhiteSpace(args))
            {
                throw new ArgumentException(args);
            }

            var splitArgs = args.Split(" ".ToCharArray(), 4);
            if (splitArgs.Length != 4)
            {
                throw new ArgumentException(args);
            }

            this.NewUserName = splitArgs[0];
            this.Mode = int.Parse(splitArgs[1]);
            this.RealName = splitArgs[3].Substring(1);
        }

        public UserMessage(string username, int mode, string realname, Guid id = new Guid()) : base(null, id)
        {
            this.NewUserName = username;
            this.Mode = mode;
            this.RealName = realname;
        }
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;
        public int Mode { get; }
        public string NewUserName { get; }
        public override string RawMessage => $"USER {this.NewUserName} {this.Mode} * :{this.RealName}";
        public string RealName { get; }
        #endregion
    }
}