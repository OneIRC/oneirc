﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class PassMessage : IRCMessage
    {
        #region Constructors and Destructors
        public PassMessage(string args, string prefix) : base(prefix)
        {
            this.Pass = args;
        }

        public PassMessage(string pass, Guid id = new Guid()) : base(null, id)
        {
            this.Pass = pass;
        }
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => false;
        public string Pass { get; }
        public override string RawMessage => $"PASS {this.Pass}";
        #endregion
    }
}