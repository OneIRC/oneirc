﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class SQuitMessage : IRCMessage
    {
        #region Constructors and Destructors
        public SQuitMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public SQuitMessage(string target, string message, Guid id = new Guid()) : base(null, id)
        {
            this.TargetServer = target;
            this.Message = message;
        }
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;
        public string Message { get; }

        public override string RawMessage { get { throw new NotImplementedException(); } }

        public string TargetServer { get; }
        #endregion
    }
}