﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class ServiceMessage : IRCMessage
    {
        #region Constructors and Destructors
        public ServiceMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public ServiceMessage(string nick, string distribution, string info, Guid id = new Guid()) : base(null, id)
        {
            this.NewNick = nick;
            this.Distribution = distribution;
            this.Info = info;
        }
        #endregion

        #region Public Properties
        public string Distribution { get; }
        public string Info { get; }

        public override bool IsGlobalMessage => true;
        public string NewNick { get; }

        public override string RawMessage { get { throw new NotImplementedException(); } }
        #endregion
    }
}