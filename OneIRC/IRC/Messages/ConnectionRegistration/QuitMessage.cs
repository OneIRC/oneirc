﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class QuitMessage : IRCMessage
    {
        #region Constructors and Destructors
        public QuitMessage(string args, string prefix) : base(prefix)
        {
            this.Message = args.Substring(1);
        }

        public QuitMessage(string message, Guid id = new Guid()) : base(null, id)
        {
            this.Message = message;
        }
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;
        public string Message { get; }
        public override string RawMessage => $"QUIT :{this.Message}";
        #endregion
    }
}