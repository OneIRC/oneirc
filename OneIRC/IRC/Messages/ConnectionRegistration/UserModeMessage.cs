﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class UserModeMessage : IRCMessage
    {
        #region Constructors and Destructors
        public UserModeMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public UserModeMessage(string nick, UserMode mode, UserModeChangeType type, Guid id = new Guid())
            : base(null, id)
        {
            this.NickName = nick;
            this.Mode = mode;
            this.Type = type;
        }
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;
        public UserMode Mode { get; }

        public override string RawMessage { get { throw new NotImplementedException(); } }

        public UserModeChangeType Type { get; }
        #endregion
    }

    public enum UserMode
    {
        Away,

        Invisible,

        Wallops,

        Restricted,

        Operator,

        LocalOperator,

        ServerNoticeRecipient
    }

    public enum UserModeChangeType
    {
        Add,

        Remove
    }
}