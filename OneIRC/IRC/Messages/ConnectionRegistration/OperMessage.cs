﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class OperMessage : IRCMessage
    {
        #region Constructors and Destructors
        public OperMessage(string args, string prefix) : base(prefix)
        {
            throw new NotImplementedException();
        }

        public OperMessage(string name, string pass, Guid id = new Guid()) : base(null, id)
        {
            this.Name = name;
            this.Pass = pass;
        }
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;
        public string Name { get; }
        public string Pass { get; }
        public override string RawMessage => $"OPER {this.Name} {this.Pass}";
        #endregion
    }
}