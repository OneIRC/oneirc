﻿namespace OneIRC.IRC.Messages
{
    #region
    using System;

    #endregion

    public class NickMessage : IRCMessage
    {
        #region Constructors and Destructors
        public NickMessage(string args, string prefix) : base(prefix)
        {
            this.NewNickName = args;
        }

        public NickMessage(string nickname, Guid id = new Guid()) : base(null, id)
        {
            this.NewNickName = nickname;
        }
        #endregion

        #region Public Properties
        public override bool IsGlobalMessage => true;
        public string NewNickName { get; }
        public override string RawMessage => $"NICK {this.NewNickName}";
        #endregion
    }
}