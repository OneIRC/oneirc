﻿namespace OneIRC.IRC
{
    #region
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading;

    using OneIRC.IRC.Messages;
    using OneIRC.IRC.Messages.Numbers;

    #endregion

    public class IRCPrivateChannel : IRCChannel
    {
        #region Constructors and Destructors
        public IRCPrivateChannel(IRCClient parent, string identifier) : base(parent, identifier)
        {
            this.Nicks = new ObservableCollection<string>();

            this.NewNicks = new SortedSet<string>();
        }
        #endregion

        #region Public Properties
        public ObservableCollection<string> Nicks { get; }
        #endregion

        #region Properties
        protected ISet<string> NewNicks { get; }
        #endregion

        #region Public Methods and Operators
        public override void HandleMessage(IRCMessage message)
        {
            if (message is Num353Message)
            {
                this.Handle353(message);
            }
            else if (message is Num366Message)
            {
                this.Handle366(null);
            }
            else if (message is JoinMessage)
            {
                this.HandleJoin(message);
            }
            else if (message is PartMessage && ((PartMessage)message).NickName != this.Parent.Nick)
            {
                this.HandlePart(message);
            }
            else if (message is PartMessage && ((PartMessage)message).NickName == this.Parent.Nick)
            {
                this.Parent.LeaveChannel(this);
            }
            else
            {
                base.HandleMessage(message);
            }
        }
        #endregion

        #region Methods
        protected void Handle353(IRCMessage message)
        {
            var msg = message as Num353Message;

            if (msg != null)
            {
                lock (this.NewNicks)
                {
                    foreach (var nick in msg.Nicks)
                    {
                        this.NewNicks.Add(nick);
                    }
                }
            }
        }

        protected void Handle366(object obj)
        {
            if (SynchronizationContext.Current != this.Parent.UIContext)
            {
                this.Parent.UIContext.Post(this.Handle366, null);
            }
            else
            {
                lock (this.NewNicks)
                {
                    this.Nicks.Clear();

                    foreach (var nick in this.NewNicks)
                    {
                        this.Nicks.Add(nick);
                    }

                    this.NewNicks.Clear();
                }
            }
        }

        protected void HandleJoin(object message)
        {
            var msg = message as JoinMessage;

            if (msg != null)
            {
                if (SynchronizationContext.Current != this.Parent.UIContext)
                {
                    this.Parent.UIContext.Post(this.HandleJoin, msg);
                }
                else
                {
                    if (!this.Nicks.Contains(msg.NickName))
                    {
                        var pos = this.Nicks.Count(n => string.Compare(msg.NickName, n, StringComparison.Ordinal) > 0);
                        this.Nicks.Insert(pos, msg.NickName);
                    }
                }
            }
        }

        protected void HandlePart(object message)
        {
            var msg = message as PartMessage;

            if (msg != null)
            {
                if (SynchronizationContext.Current != this.Parent.UIContext)
                {
                    this.Parent.UIContext.Post(this.HandlePart, msg);
                }
                else
                {
                    this.Nicks.Remove(msg.NickName);
                }
            }
        }
        #endregion
    }
}