﻿namespace OneIRC.IRC
{
    #region
    using System;
    using System.Collections.ObjectModel;
    using System.Threading;

    using OneIRC.IRC.Messages;

    #endregion

    public class IRCChannel
    {
        #region Constructors and Destructors
        public IRCChannel(IRCClient parent, string identifier)
        {
            this.Identifier = identifier;
            this.Messages = new ObservableCollection<IRCMessage>();
            this.Parent = parent;
        }
        #endregion

        #region Public Properties
        public string Identifier { get; private set; }
        public ObservableCollection<IRCMessage> Messages { get; }
        #endregion

        #region Properties
        protected IRCClient Parent { get; }
        #endregion

        #region Public Methods and Operators
        public virtual void HandleMessage(IRCMessage message)
        {
            this.AddMessageToQueue(message);
        }

        public virtual void MessageSent(Guid id)
        {
            // TODO
        }

        public virtual void Send(IRCMessage message)
        {
            message.NickName = this.Parent.Nick;

            this.Parent.SendMessage(message, this);
        }
        #endregion

        #region Methods
        protected void AddMessageToQueue(object message)
        {
            var msg = message as IRCMessage;

            if (msg != null)
            {
                if (SynchronizationContext.Current != this.Parent.UIContext)
                {
                    this.Parent.UIContext.Post(this.AddMessageToQueue, msg);
                }
                else
                {
                    this.HandleIRCMessage(msg);
                }
            }
        }

        protected virtual void HandleIRCMessage(IRCMessage message)
        {
            this.Messages.Add(message);
        }
        #endregion
    }
}