﻿namespace OneIRC.IRC
{
    public class IRCClientConfig
    {
        #region Constructors and Destructors
        public IRCClientConfig(IRCServerConfig serverConfig, string nick, string username)
        {
            this.ServerConfig = serverConfig;
            this.Nick = nick;
            this.Username = username;
        }
        #endregion

        #region Public Properties
        public string Nick { get; }
        public IRCServerConfig ServerConfig { get; }
        public string Username { get; }
        #endregion
    }
}