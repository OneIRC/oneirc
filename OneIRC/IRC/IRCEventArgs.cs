﻿namespace OneIRC.IRC
{
    #region
    using System;

    #endregion

    public class IRCEventArgs : EventArgs
    {
        #region Constructors and Destructors
        public IRCEventArgs()
        {
            this.Time = DateTime.Now;
        }

        public IRCEventArgs(Guid id) : this()
        {
            this.CorrelationId = id;
        }
        #endregion

        #region Public Properties
        public Guid CorrelationId { get; private set; }
        public string Message { get; set; }
        public IRCConnectionStatus Status { get; set; }
        public DateTime Time { get; private set; }
        #endregion
    }
}