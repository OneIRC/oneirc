﻿// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace OneIRC
{
    #region
    using System;
    using System.Windows.Input;

    using Windows.System;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Input;

    using OneIRC.IRC;
    using OneIRC.IRC.Messages;

    #endregion

    public sealed partial class RawRoom : UserControl
    {
        #region Static Fields

        // Using a DependencyProperty as the backing store for Channel.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ChannelProperty = DependencyProperty.Register(
                                                                                                "Channel",
                                                                                                typeof(IRCChannel),
                                                                                                typeof(RawRoom),
                                                                                                new PropertyMetadata(
                                                                                                    null,
                                                                                                    OnChannelChanged));
        #endregion

        #region Constructors and Destructors
        public RawRoom()
        {
            this.InitializeComponent();
        }
        #endregion

        #region Public Properties
        public IRCChannel Channel
        {
            get { return (IRCChannel)this.GetValue(ChannelProperty); }
            set { this.SetValue(ChannelProperty, value); }
        }
        #endregion

        #region Methods
        private static void OnChannelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var room = (RawRoom)d;

            room.SubmitButton.Command = new ChatSendCallBack((IRCChannel)e.NewValue, room.TextBox);
        }

        private void MessageKeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                var text = this.TextBox.Text;

                if (this.SubmitButton.Command?.CanExecute(text) == true)
                {
                    this.SubmitButton.Command.Execute(text);
                }
            }
        }
        #endregion

        private class ChatSendCallBack : ICommand
        {
            #region Constructors and Destructors
            public ChatSendCallBack(IRCChannel parent, TextBox textBox)
            {
                this.Parent = parent;
                this.TextBox = textBox;
            }
            #endregion

            #region Public Events
            public event EventHandler CanExecuteChanged;
            #endregion

            #region Properties
            private IRCChannel Parent { get; }
            private TextBox TextBox { get; }
            #endregion

            #region Public Methods and Operators
            public bool CanExecute(object parameter)
            {
                return !string.IsNullOrEmpty(parameter as string);
            }

            public void Execute(object parameter)
            {
                this.Parent.Send(IRCMessage.Parse(parameter as string));

                this.TextBox.Text = string.Empty;
            }
            #endregion
        }
    }
}