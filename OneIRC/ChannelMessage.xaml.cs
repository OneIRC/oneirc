﻿namespace OneIRC
{
    #region
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Documents;

    #endregion

    public sealed partial class ChannelMessage : UserControl
    {
        #region Static Fields
        public static readonly DependencyProperty MessageContentsProperty =
            DependencyProperty.Register(
                                        "MessageContents",
                                        typeof(string),
                                        typeof(ChannelMessage),
                                        new PropertyMetadata("", MessageContentsChanged));
        #endregion

        #region Constructors and Destructors
        public ChannelMessage()
        {
            this.InitializeComponent();
        }
        #endregion

        #region Public Properties
        public string MessageContents
        {
            get { return (string)this.GetValue(MessageContentsProperty); }
            set { this.SetValue(MessageContentsProperty, value); }
        }
        #endregion

        #region Properties
        private static Regex Http => new Regex(@"(http|ftp|https):\/\/.*");
        private static Regex Picture
            =>
                new Regex(
                    @"(((http|https):\/\/)?[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?).(bmp|gif|jpg|jpeg|png)",
                    RegexOptions.Compiled);
        private static Regex Url
            =>
                new Regex(
                    @"(((http|ftp|https):\/\/)?[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?)",
                    RegexOptions.Compiled);
        #endregion

        #region Methods
        private static IEnumerable<Uri> HandleHyperlinks(Paragraph paragraph)
        {
            var runs = paragraph.Inlines.Select(i => i as Run).Where(r => r != null).ToList();
            var pics = new List<Uri>();

            foreach (var run in runs)
            {
                var split = false;
                var index = paragraph.Inlines.IndexOf(run);
                var last = 0;
                foreach (Match match in Url.Matches(run.Text))
                {
                    var link = MakeHyperlink(match.Value);

                    paragraph.Inlines.Insert(index++, new Run { Text = run.Text.Substring(last, match.Index - last) });
                    paragraph.Inlines.Insert(index++, link);

                    split = true;
                    last = match.Index + match.Length;

                    if (Picture.IsMatch(link.NavigateUri?.AbsoluteUri))
                    {
                        pics.Add(link.NavigateUri);
                    }
                }

                if (split)
                {
                    paragraph.Inlines.Remove(run);
                }
            }

            return pics;
        }

        private static Hyperlink MakeHyperlink(string uri)
        {
            var run = new Run { Text = uri };

            if (!Http.IsMatch(uri))
            {
                uri = $"http://{uri}";
            }
            var link = new Hyperlink { NavigateUri = new Uri(uri) };

            link.Inlines.Add(run);

            return link;
        }

        private static void MessageContentsChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            var paragraph = new Paragraph();
            var target = sender as ChannelMessage;

            if (target == null)
            {
                return;
            }

            paragraph.Inlines.Add(new Run { Text = args.NewValue.ToString() });

            var pics = HandleHyperlinks(paragraph);

            target.TextBlock.Blocks.Clear();
            target.TextBlock.Blocks.Add(paragraph);

            target.Images.ItemsSource = ((App)Application.Current).UserSettings.InternalSettings.ExpandImages
                                            ? pics
                                            : null;
        }
        #endregion
    }
}