﻿namespace OneIRC
{
    #region
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;

    using Windows.System.Profile;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;

    #endregion

    public sealed partial class StageLeftMenu : UserControl
    {
        #region Static Fields
        public static readonly DependencyProperty BottomMenuItemsProperty =
            DependencyProperty.Register(
                                        "BottomMenuItems",
                                        typeof(ObservableCollection<StageLeftMenuItem>),
                                        typeof(StageLeftMenu),
                                        null);

        public static readonly DependencyProperty IsPaneOpenProperty = DependencyProperty.Register(
                                                                                                   "IsPaneOpen",
                                                                                                   typeof(bool),
                                                                                                   typeof(StageLeftMenu),
                                                                                                   null);

        public static readonly DependencyProperty MainContentProperty = DependencyProperty.Register(
                                                                                                    "MainContent",
                                                                                                    typeof(object),
                                                                                                    typeof(StageLeftMenu
                                                                                                        ),
                                                                                                    null);

        public static readonly DependencyProperty MinimizedHeightProperty =
            DependencyProperty.Register("MinimizedHeight", typeof(RowDefinition), typeof(StageLeftMenu), null);

        public static readonly DependencyProperty MinimizedWidthProperty = DependencyProperty.Register(
                                                                                                       "MinimizedWidth",
                                                                                                       typeof(
                                                                                                           ColumnDefinition
                                                                                                           ),
                                                                                                       typeof(
                                                                                                           StageLeftMenu
                                                                                                           ),
                                                                                                       null);

        public static readonly DependencyProperty TopMenuItemsProperty = DependencyProperty.Register(
                                                                                                     "TopMenuItems",
                                                                                                     typeof(
                                                                                                         ObservableCollection
                                                                                                         <
                                                                                                         StageLeftMenuItem
                                                                                                         >),
                                                                                                     typeof(
                                                                                                         StageLeftMenu),
                                                                                                     null);
        #endregion

        #region Constructors and Destructors
        public StageLeftMenu()
        {
            this.InitializeComponent();

            this.SetupPhone();
            this.InnerMenu.ItemClickedHandler += this.HandleItemClicked;
            this.TopMenuItems = new ObservableCollection<StageLeftMenuItem>();
            this.BottomMenuItems = new ObservableCollection<StageLeftMenuItem>();
        }
        #endregion

        #region Public Properties
        public ObservableCollection<StageLeftMenuItem> BottomMenuItems
        {
            get { return (ObservableCollection<StageLeftMenuItem>)this.GetValue(BottomMenuItemsProperty); }
            set
            {
                this.SetValue(BottomMenuItemsProperty, value);
                this.InnerMenu.BottomMenuItems = this.BottomMenuItems;
            }
        }

        public bool IsPaneOpen
        {
            get { return (bool)this.GetValue(IsPaneOpenProperty); }
            set
            {
                this.SetValue(IsPaneOpenProperty, value);
                this.MenuPane.IsPaneOpen = this.IsPaneOpen;
            }
        }

        public object MainContent
        {
            get { return this.GetValue(MainContentProperty); }
            set { this.SetValue(MainContentProperty, value); }
        }

        public RowDefinition MinimizedHeight
        {
            get { return (RowDefinition)this.GetValue(MinimizedHeightProperty); }
            set { this.SetValue(MinimizedHeightProperty, value); }
        }

        public ColumnDefinition MinimizedWidth
        {
            get { return (ColumnDefinition)this.GetValue(MinimizedWidthProperty); }
            set { this.SetValue(MinimizedWidthProperty, value); }
        }

        public ObservableCollection<StageLeftMenuItem> TopMenuItems
        {
            get { return (ObservableCollection<StageLeftMenuItem>)this.GetValue(TopMenuItemsProperty); }
            set
            {
                this.SetValue(TopMenuItemsProperty, value);
                this.InnerMenu.TopMenuItems = this.TopMenuItems;
            }
        }
        #endregion

        #region Public Methods and Operators
        public void OpenMenuDrawerIfExpanded(object item)
        {
            if (this.MenuPane.IsPaneOpen)
            {
                foreach (var menuItem in this.TopMenuItems.Where(i => i.MenuContent == item))
                {
                    this.InnerMenu.ShowItem(menuItem);
                }
                foreach (var menuItem in this.BottomMenuItems.Where(i => i.MenuContent == item))
                {
                    this.InnerMenu.ShowItem(menuItem);
                }
            }
        }
        #endregion

        #region Methods
        private void HamburgerButtonClick(object sender, RoutedEventArgs args)
        {
            this.MenuPane.IsPaneOpen = !this.MenuPane.IsPaneOpen;
        }

        private void HandleItemClicked(object sender, EventArgs args)
        {
            this.MenuPane.IsPaneOpen = true;
        }

        private void PaneClosingClick(SplitView sender, SplitViewPaneClosingEventArgs args)
        {
            this.InnerMenu.CollapseItems();
        }

        private void SetupPhone()
        {
            if (!AnalyticsInfo.VersionInfo.DeviceFamily.Contains("Desktop"))
            {
                this.DesktopMenuButton.Visibility = Visibility.Collapsed;

                this.MenuPane.CompactPaneLength = 0;
            }
        }
        #endregion
    }
}