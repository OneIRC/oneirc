﻿namespace OneIRC
{
    #region
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Linq;

    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;

    #endregion

    public sealed partial class StageLeftInnerMenu : UserControl
    {
        #region Static Fields
        public static readonly DependencyProperty BottomMenuItemsProperty =
            DependencyProperty.Register(
                                        "BottomMenuItems",
                                        typeof(ObservableCollection<StageLeftMenuItem>),
                                        typeof(StageLeftInnerMenu),
                                        null);

        public static readonly DependencyProperty ItemClickedHandlerProperty =
            DependencyProperty.Register(
                                        "ItemClickedHandler",
                                        typeof(EventHandler<EventArgs>),
                                        typeof(StageLeftInnerMenu),
                                        null);

        public static readonly DependencyProperty TopMenuItemsProperty = DependencyProperty.Register(
                                                                                                     "TopMenuItems",
                                                                                                     typeof(
                                                                                                         ObservableCollection
                                                                                                         <
                                                                                                         StageLeftMenuItem
                                                                                                         >),
                                                                                                     typeof(
                                                                                                         StageLeftInnerMenu
                                                                                                         ),
                                                                                                     null);
        #endregion

        #region Constructors and Destructors
        public StageLeftInnerMenu()
        {
            this.InitializeComponent();

            this.BufferAfterItem = new Dictionary<StageLeftMenuItem, RowDefinition>();
            this.TopMenuItems = new ObservableCollection<StageLeftMenuItem>();
            this.BottomMenuItems = new ObservableCollection<StageLeftMenuItem>();
        }
        #endregion

        #region Public Properties
        public ObservableCollection<StageLeftMenuItem> BottomMenuItems
        {
            get { return (ObservableCollection<StageLeftMenuItem>)this.GetValue(BottomMenuItemsProperty); }
            set
            {
                if (this.BottomMenuItems != null)
                {
                    this.BottomMenuItems.CollectionChanged -= this.MenuItemsChanged;
                }

                value.CollectionChanged += this.MenuItemsChanged;

                this.SetValue(BottomMenuItemsProperty, value);
                this.SetupMenu();
            }
        }

        public EventHandler<EventArgs> ItemClickedHandler
        {
            get { return (EventHandler<EventArgs>)this.GetValue(ItemClickedHandlerProperty); }
            set { this.SetValue(ItemClickedHandlerProperty, value); }
        }

        public ObservableCollection<StageLeftMenuItem> TopMenuItems
        {
            get { return (ObservableCollection<StageLeftMenuItem>)this.GetValue(TopMenuItemsProperty); }
            set
            {
                if (this.TopMenuItems != null)
                {
                    this.TopMenuItems.CollectionChanged -= this.MenuItemsChanged;
                }

                value.CollectionChanged += this.MenuItemsChanged;

                this.SetValue(TopMenuItemsProperty, value);
                this.SetupMenu();
            }
        }
        #endregion

        #region Properties
        private IDictionary<StageLeftMenuItem, RowDefinition> BufferAfterItem { get; set; }

        private RowDefinition MainBuffer { get; set; }
        #endregion

        #region Public Methods and Operators
        public void CollapseItems()
        {
            this.CollapseExcept(this.MainBuffer);
            this.CollapseExcept((StageLeftMenuItem)null);
        }

        public void ShowItem(StageLeftMenuItem item)
        {
            this.CollapseExcept(this.BufferAfterItem[item]);
            this.CollapseExcept(item);
        }
        #endregion

        #region Methods
        private void AddItem(IList<RowDefinition> rowDefs, IList<ContentPresenter> childDefs, StageLeftMenuItem item)
        {
            this.BufferAfterItem[item] = new RowDefinition { Height = GridLength.Auto };
            var child = new ContentPresenter { Content = item };
            Grid.SetRow(child, rowDefs.Count);
            childDefs.Add(child);

            rowDefs.Add(new RowDefinition { Height = GridLength.Auto });
            rowDefs.Add(this.BufferAfterItem[item]);
        }

        private void CollapseExcept(RowDefinition selected)
        {
            foreach (var row in this.BufferAfterItem.Values)
            {
                row.Height = row == selected ? new GridLength(1, GridUnitType.Star) : GridLength.Auto;
            }
        }

        private void CollapseExcept(StageLeftMenuItem selected)
        {
            foreach (var item in this.BufferAfterItem.Keys)
            {
                item.ContentDrawerVisibility = item == selected ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        private void HandleItemClicked(object sender, EventArgs args)
        {
            var modified = sender as StageLeftMenuItem;

            if (modified != null && modified.ContentDrawerVisibility == Visibility.Visible)
            {
                this.ShowItem(modified);
                this.ItemClickedHandler?.Invoke(sender, args);
            }
            else if (this.BufferAfterItem.Keys.All(i => i.ContentDrawerVisibility == Visibility.Collapsed))
            {
                this.CollapseItems();
            }
        }

        private void MenuItemsChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            this.SetupMenu();

            switch (args.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var item in args.NewItems)
                    {
                        var menu = item as StageLeftMenuItem;

                        if (menu != null)
                        {
                            menu.ContentVisibilityHandler += this.HandleItemClicked;
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                case NotifyCollectionChangedAction.Replace:
                case NotifyCollectionChangedAction.Reset:
                    foreach (var item in args.OldItems)
                    {
                        var menu = item as StageLeftMenuItem;

                        if (menu != null)
                        {
                            menu.ContentVisibilityHandler -= this.HandleItemClicked;
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Move:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SetupMenu()
        {
            var rowDefs = new List<RowDefinition>();
            var childDefs = new List<ContentPresenter>();

            foreach (var item in this.TopMenuItems ?? new ObservableCollection<StageLeftMenuItem>())
            {
                this.AddItem(rowDefs, childDefs, item);
            }

            if (!rowDefs.Any())
            {
                rowDefs.Add(new RowDefinition { Height = GridLength.Auto });
            }
            this.MainBuffer = rowDefs.Last();
            this.BufferAfterItem[new StageLeftMenuItem()] = this.MainBuffer;

            foreach (var item in this.BottomMenuItems ?? new ObservableCollection<StageLeftMenuItem>())
            {
                this.AddItem(rowDefs, childDefs, item);
            }

            this.MainGrid.Children.Clear();
            foreach (var child in childDefs)
            {
                this.MainGrid.Children.Add(child);
            }

            this.MainGrid.RowDefinitions.Clear();
            foreach (var row in rowDefs)
            {
                this.MainGrid.RowDefinitions.Add(row);
            }

            this.CollapseItems();
        }
        #endregion
    }
}