﻿// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace OneIRC
{
    #region
    using System;

    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Input;

    #endregion

    public sealed partial class StageLeftMenuItem : UserControl
    {
        #region Static Fields
        public static readonly DependencyProperty ContentDrawerVisibilityProperty =
            DependencyProperty.Register("ContentDrawerVisibility", typeof(Visibility), typeof(StageLeftMenuItem), null);

        public static readonly DependencyProperty IconProperty = DependencyProperty.Register(
                                                                                             "Icon",
                                                                                             typeof(object),
                                                                                             typeof(StageLeftMenuItem),
                                                                                             null);

        public static readonly DependencyProperty LabelProperty = DependencyProperty.Register(
                                                                                              "Label",
                                                                                              typeof(string),
                                                                                              typeof(StageLeftMenuItem),
                                                                                              null);

        public static readonly DependencyProperty MenuContentProperty = DependencyProperty.Register(
                                                                                                    "MenuContent",
                                                                                                    typeof(object),
                                                                                                    typeof(
                                                                                                        StageLeftMenuItem
                                                                                                        ),
                                                                                                    null);

        public static readonly DependencyProperty MinimizedHeightProperty =
            DependencyProperty.Register("MinimizedHeight", typeof(RowDefinition), typeof(StageLeftMenuItem), null);

        public static readonly DependencyProperty MinimizedWidthProperty = DependencyProperty.Register(
                                                                                                       "MinimizedWidth",
                                                                                                       typeof(
                                                                                                           ColumnDefinition
                                                                                                           ),
                                                                                                       typeof(
                                                                                                           StageLeftMenuItem
                                                                                                           ),
                                                                                                       null);
        #endregion

        #region Constructors and Destructors
        public StageLeftMenuItem()
        {
            this.InitializeComponent();
        }
        #endregion

        #region Public Events
        public event EventHandler<EventArgs> ContentVisibilityHandler;
        #endregion

        #region Public Properties
        public Visibility ContentDrawerVisibility
        {
            get { return (Visibility)this.GetValue(ContentDrawerVisibilityProperty); }
            set
            {
                switch (value)
                {
                    case Visibility.Visible:
                        this.ContentRow.Height = OneStar;
                        break;
                    default:
                    case Visibility.Collapsed:
                        this.ContentRow.Height = ZeroStar;
                        break;
                }

                var isUpdate = value != this.ContentDrawerVisibility;
                this.SetValue(ContentDrawerVisibilityProperty, value);

                if (isUpdate)
                {
                    this.ContentVisibilityHandler?.Invoke(this, new EventArgs());
                }
            }
        }

        public object Icon
        {
            get { return this.GetValue(IconProperty); }
            set { this.SetValue(IconProperty, value); }
        }

        public string Label
        {
            get { return (string)this.GetValue(LabelProperty); }
            set { this.SetValue(LabelProperty, value); }
        }

        public object MenuContent
        {
            get { return this.GetValue(MenuContentProperty); }
            set { this.SetValue(MenuContentProperty, value); }
        }

        public RowDefinition MinimizedHeight
        {
            get { return (RowDefinition)this.GetValue(MinimizedHeightProperty); }
            set { this.SetValue(MinimizedHeightProperty, value); }
        }

        public ColumnDefinition MinimizedWidth
        {
            get { return (ColumnDefinition)this.GetValue(MinimizedWidthProperty); }
            set { this.SetValue(MinimizedWidthProperty, value); }
        }
        #endregion

        #region Properties
        private static GridLength OneStar => new GridLength(1, GridUnitType.Star);
        private static GridLength ZeroStar => new GridLength(0, GridUnitType.Pixel);
        #endregion

        #region Methods
        private void ExpandClick(object sender, TappedRoutedEventArgs e)
        {
            this.ContentDrawerVisibility = this.ContentDrawerVisibility == Visibility.Visible
                                               ? Visibility.Collapsed
                                               : Visibility.Visible;
        }
        #endregion
    }
}