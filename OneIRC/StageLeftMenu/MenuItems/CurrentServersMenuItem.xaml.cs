﻿namespace OneIRC
{
    #region
    using System;
    using System.Collections.Specialized;
    using System.Linq;

    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Controls.Primitives;
    using Windows.UI.Xaml.Input;

    using OneIRC.IRC;

    #endregion

    public sealed partial class CurrentServersMenuItem : UserControl
    {
        #region Static Fields
        public static readonly DependencyProperty StageLeftMenuProperty = DependencyProperty.Register(
                                                                                                      "StageLeftMenu",
                                                                                                      typeof(
                                                                                                          StageLeftMenu),
                                                                                                      typeof(
                                                                                                          CurrentServersMenuItem
                                                                                                          ),
                                                                                                      null);
        #endregion

        #region Constructors and Destructors
        public CurrentServersMenuItem()
        {
            this.InitializeComponent();

            this.CurrentServers.ItemsSource = ((App)Application.Current).ConnectedClients;
            this.CurrentServers.SelectionChanged += this.SelectionChanged;

            ((App)Application.Current).ConnectedClients.CollectionChanged += this.ConnectedClientsChangedHandler;
        }
        #endregion

        #region Public Properties
        public EventHandler<SelectionChangedEventArgs> SelectionChangedHandler { get; set; }

        public StageLeftMenu StageLeftMenu
        {
            get { return (StageLeftMenu)this.GetValue(StageLeftMenuProperty); }
            set { this.SetValue(StageLeftMenuProperty, value); }
        }
        #endregion

        #region Methods
        private void AddToFavoritesClick(object sender, TappedRoutedEventArgs e)
        {
            var config = ((sender as Control)?.DataContext as IRCClientConfig);
            var favorites = (Application.Current as App)?.UserSettings.FavoriteClients;

            if (config != null && favorites != null && !favorites.Contains(config))
            {
                favorites.Add(config);
            }
        }

        private void ConnectedClientsChangedHandler(object sender, NotifyCollectionChangedEventArgs args)
        {
            switch (args.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var item in args.NewItems)
                    {
                        var client = item as IRCClient;

                        if (client != null)
                        {
                            this.SelectServer(client);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                case NotifyCollectionChangedAction.Replace:
                case NotifyCollectionChangedAction.Reset:
                case NotifyCollectionChangedAction.Move:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void LeaveServerClick(object sender, TappedRoutedEventArgs e)
        {
            var config = (sender as Control)?.DataContext as IRCClientConfig;

            this.RemoveIfExistingConfig(config);
        }

        private void ListRightTapped(object sender, RightTappedRoutedEventArgs e)
        {
            FlyoutBase.ShowAttachedFlyout(sender as FrameworkElement);
        }

        private void RemoveIfExistingConfig(IRCClientConfig config)
        {
            var app = ((App)Application.Current);

            if (config != null && app.ConnectedClients.Any(c => c.Config == config))
            {
                var client = app.ConnectedClients.First(c => c.Config == config);
                client.Close();
                app.ConnectedClients.Remove(client);
            }
        }

        private void SelectionChanged(object sender, SelectionChangedEventArgs args)
        {
            this.StageLeftMenu?.OpenMenuDrawerIfExpanded(this);

            this.SelectionChangedHandler?.Invoke(sender, args);
        }

        private void SelectServer(IRCClient client)
        {
            this.CurrentServers.SelectedItem = client;
        }
        #endregion
    }
}