﻿namespace OneIRC
{
    #region
    using System.Threading;

    using Windows.System;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Input;

    using OneIRC.IRC;

    #endregion

    public sealed partial class NewServerMenuItem : UserControl
    {
        #region Constructors and Destructors
        public NewServerMenuItem()
        {
            this.InitializeComponent();
        }
        #endregion

        #region Methods
        private void JoinServerClick(object sender, TappedRoutedEventArgs e)
        {
            var app = ((App)Application.Current);

            var serverConfig = new IRCServerConfig(this.NewServerUrl.Text);
            var clientConfig = new IRCClientConfig(
                serverConfig,
                this.NewServerNickname.Text,
                this.NewServerUsername.Text);

            app.AddIfNewConfig(clientConfig);

            this.NewServerUrl.Text = string.Empty;
            this.NewServerNickname.Text = string.Empty;
            this.NewServerUsername.Text = string.Empty;
        }

        private void NewServerKeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                this.JoinServerClick(sender, new TappedRoutedEventArgs());
            }
        }
        #endregion
    }
}