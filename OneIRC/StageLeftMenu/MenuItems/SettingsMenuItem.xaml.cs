﻿namespace OneIRC
{
    #region
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;

    #endregion

    public sealed partial class SettingsMenuItem : UserControl
    {
        #region Static Fields
        public static readonly DependencyProperty SettingsProperty = DependencyProperty.Register(
                                                                                                 "Settings",
                                                                                                 typeof(UserSettings),
                                                                                                 typeof(SettingsMenuItem
                                                                                                     ),
                                                                                                 null);
        #endregion

        #region Constructors and Destructors
        public SettingsMenuItem()
        {
            this.InitializeComponent();

            this.Settings = ((App)Application.Current).UserSettings;
        }
        #endregion

        #region Public Properties
        public UserSettings Settings
        {
            get { return (UserSettings)this.GetValue(SettingsProperty); }
            set { this.SetValue(SettingsProperty, value); }
        }
        #endregion
    }
}