﻿namespace OneIRC
{
    #region
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Controls.Primitives;
    using Windows.UI.Xaml.Input;

    using OneIRC.IRC;

    #endregion

    public sealed partial class FavoriteServersMenuItem : UserControl
    {
        #region Constructors and Destructors
        public FavoriteServersMenuItem()
        {
            this.InitializeComponent();

            this.FavoriteServers.ItemsSource = ((App)Application.Current).UserSettings.FavoriteClients;
        }
        #endregion

        #region Methods
        private void FavoriteServerClick(object sender, SelectionChangedEventArgs e)
        {
            var app = ((App)Application.Current);
            this.FavoriteServers.SelectedIndex = -1;

            foreach (var config in e.AddedItems)
            {
                app.AddIfNewConfig(config as IRCClientConfig);
            }
        }

        private void ListRightTapped(object sender, RightTappedRoutedEventArgs e)
        {
            FlyoutBase.ShowAttachedFlyout(sender as FrameworkElement);
        }

        private void RemoveFromFavoritesClick(object sender, TappedRoutedEventArgs e)
        {
            var config = ((sender as Control)?.DataContext as IRCClientConfig);
            var favorites = (Application.Current as App)?.UserSettings.FavoriteClients;

            if (config != null)
            {
                favorites?.Remove(config);
            }
        }
        #endregion
    }
}