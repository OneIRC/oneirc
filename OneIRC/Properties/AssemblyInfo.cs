﻿using System.Resources;
#region
using System.Reflection;
using System.Runtime.InteropServices;

#endregion

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("OneIRC")]
[assembly: AssemblyDescription("Simple, Beautiful, Open-Source IRC for Windows")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Zach Dylag")]
[assembly: AssemblyProduct("OneIRC")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("0.0.3.0")]
[assembly: AssemblyFileVersion("0.0.3.0")]
[assembly: ComVisible(false)]
[assembly: NeutralResourcesLanguage("en")]

