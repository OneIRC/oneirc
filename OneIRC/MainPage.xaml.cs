﻿namespace OneIRC
{
    #region
    using Windows.System.Profile;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;

    #endregion

    public sealed partial class MainPage : Page
    {
        #region Constructors and Destructors
        public MainPage()
        {
            this.InitializeComponent();

            this.CurrentServers.StageLeftMenu = this.StageLeft;
            this.CurrentServers.SelectionChangedHandler += this.ClientsView.ClientSelectionChangedHandler;
            this.NewRoomPopup.ClientView = this.ClientsView;

            this.SetupPhone();
        }
        #endregion

        #region Methods
        private void HamburgerButtonClick(object sender, RoutedEventArgs args)
        {
            this.StageLeft.IsPaneOpen = !this.StageLeft.IsPaneOpen;
        }

        private void SetupPhone()
        {
            if (!AnalyticsInfo.VersionInfo.DeviceFamily.Contains("Desktop"))
            {
                this.PhoneMenuButton.Visibility = Visibility.Visible;
            }
        }
        #endregion
    }
}