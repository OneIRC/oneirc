﻿namespace OneIRC
{
    #region
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.IO;
    using System.Threading.Tasks;

    using Windows.Storage;

    using Newtonsoft.Json;

    using OneIRC.IRC;

    #endregion

    public class UserSettings
    {
        #region Constructors and Destructors
        public UserSettings(string key)
        {
            this.Key = key;

            this.FavoriteClients = new ObservableCollection<IRCClientConfig>();
            this.InternalSettings = new Settings();
            this.FavoriteClients.CollectionChanged += this.FavoritesOnCollectionChanged;
            this.InternalSettings.PropertyChanged += this.SettingsOnObjectChanged;
        }
        #endregion

        #region Public Properties
        public ObservableCollection<IRCClientConfig> FavoriteClients { get; }
        public Settings InternalSettings { get; }
        #endregion

        #region Properties
        private string FavoriteClientsFileName => $"{this.Key}_FavoriteClients.json";
        private string Key { get; }
        private string SettingsFileName => $"{this.Key}_Settings.json";

        private StorageFolder Storage => ApplicationData.Current.RoamingFolder;
        #endregion

        #region Public Methods and Operators
        public async Task LoadFromFiles()
        {
            this.FavoriteClients.CollectionChanged -= this.FavoritesOnCollectionChanged;
            this.InternalSettings.PropertyChanged -= this.SettingsOnObjectChanged;

            await this.LoadSettings();
            await this.LoadFavorites();

            this.FavoriteClients.CollectionChanged += this.FavoritesOnCollectionChanged;
            this.InternalSettings.PropertyChanged += this.SettingsOnObjectChanged;
        }
        #endregion

        #region Methods
        private async void FavoritesOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            await this.SaveFavorites();
        }

        private async Task LoadFavorites()
        {
            var file =
                await this.Storage.CreateFileAsync(this.FavoriteClientsFileName, CreationCollisionOption.OpenIfExists);
            using (var stream = await file.OpenStreamForReadAsync())
            {
                using (var jsonReader = new StreamReader(stream))
                {
                    var serializer = new JsonSerializer();
                    var clients = (IRCClientConfig[])serializer.Deserialize(jsonReader, typeof(IRCClientConfig[]));

                    this.FavoriteClients.Clear();
                    foreach (var client in clients ?? new IRCClientConfig[0])
                    {
                        this.FavoriteClients.Add(client);
                    }
                }
            }
        }

        private async Task LoadSettings()
        {
            var file = await this.Storage.CreateFileAsync(this.SettingsFileName, CreationCollisionOption.OpenIfExists);
            using (var stream = await file.OpenStreamForReadAsync())
            {
                using (var jsonReader = new StreamReader(stream))
                {
                    var serializer = new JsonSerializer();
                    var settings = (Settings)serializer.Deserialize(jsonReader, typeof(Settings));

                    if (settings != null)
                    {
                        this.InternalSettings.ExpandImages = settings.ExpandImages;
                    }
                }
            }
        }

        private async Task SaveFavorites()
        {
            var file =
                await
                this.Storage.CreateFileAsync(this.FavoriteClientsFileName, CreationCollisionOption.ReplaceExisting);
            using (var stream = await file.OpenStreamForWriteAsync())
            {
                using (var jsonwriter = new StreamWriter(stream))
                {
                    var serializer = new JsonSerializer();
                    serializer.Serialize(jsonwriter, this.FavoriteClients);
                }
            }
        }

        private async Task SaveSettings()
        {
            var file =
                await this.Storage.CreateFileAsync(this.SettingsFileName, CreationCollisionOption.ReplaceExisting);
            using (var stream = await file.OpenStreamForWriteAsync())
            {
                using (var jsonwriter = new StreamWriter(stream))
                {
                    var serializer = new JsonSerializer();
                    serializer.Serialize(jsonwriter, this.InternalSettings);
                }
            }
        }

        private async void SettingsOnObjectChanged(object sender, PropertyChangedEventArgs args)
        {
            await this.SaveSettings();
        }
        #endregion
    }
}