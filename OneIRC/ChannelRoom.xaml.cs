﻿// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace OneIRC
{
    #region
    using System;
    using System.Windows.Input;

    using Windows.System;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Input;

    using OneIRC.IRC;
    using OneIRC.IRC.Messages;

    #endregion

    public sealed partial class ChannelRoom : UserControl
    {
        #region Static Fields

        // Using a DependencyProperty as the backing store for Channel.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ChannelProperty = DependencyProperty.Register(
                                                                                                "Channel",
                                                                                                typeof(IRCPrivateChannel
                                                                                                    ),
                                                                                                typeof(ChannelRoom),
                                                                                                new PropertyMetadata(
                                                                                                    null,
                                                                                                    OnChannelChanged));
        #endregion

        #region Constructors and Destructors
        public ChannelRoom()
        {
            this.InitializeComponent();
        }
        #endregion

        #region Public Properties
        public IRCPrivateChannel Channel
        {
            get { return (IRCPrivateChannel)this.GetValue(ChannelProperty); }
            set { this.SetValue(ChannelProperty, value); }
        }
        #endregion

        #region Methods
        private static void OnChannelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var room = (ChannelRoom)d;

            room.SubmitButton.Command = new ChatSendCallBack((IRCPrivateChannel)e.NewValue, room.TextBox);
        }

        private void MessageKeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                var text = this.TextBox.Text;

                if (this.SubmitButton.Command?.CanExecute(text) == true)
                {
                    this.SubmitButton.Command.Execute(text);
                }
            }
        }
        #endregion

        private class ChatSendCallBack : ICommand
        {
            #region Constructors and Destructors
            public ChatSendCallBack(IRCPrivateChannel parent, TextBox textBox)
            {
                this.Parent = parent;
                this.TextBox = textBox;
            }
            #endregion

            #region Public Events
            public event EventHandler CanExecuteChanged;
            #endregion

            #region Properties
            private IRCPrivateChannel Parent { get; }
            private TextBox TextBox { get; }
            #endregion

            #region Public Methods and Operators
            public bool CanExecute(object parameter)
            {
                return !string.IsNullOrEmpty(parameter as string);
            }

            public void Execute(object parameter)
            {
                this.Parent.Send(new PrivMessage(this.Parent.Identifier, parameter as string, Guid.NewGuid()));

                this.TextBox.Text = string.Empty;
            }
            #endregion
        }
    }
}