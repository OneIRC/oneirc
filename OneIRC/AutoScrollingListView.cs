﻿namespace OneIRC
{
    #region
    using System.Linq;

    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Media;

    #endregion

    public class AutoScrollingListView : ListView
    {
        #region Properties
        private ScrollViewer ScrollView { get; set; }
        #endregion

        #region Methods
        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.ScrollView = RecursiveVisualChildFinder<ScrollViewer>(this) as ScrollViewer;
        }

        protected override void OnItemsChanged(object item)
        {
            base.OnItemsChanged(item);

            // If we cannot scroll, or the user has manually scrolled, don't scroll
            if (this.ScrollView == null || !this.ScrollView.VerticalOffset.Equals(this.ScrollView.ScrollableHeight)
                || !this.Items.Any())
            {
                return;
            }

            this.UpdateLayout();
            this.ScrollIntoView(this.Items?[this.Items.Count - 1], ScrollIntoViewAlignment.Leading);
        }

        private static DependencyObject RecursiveVisualChildFinder<T>(DependencyObject rootObject)
        {
            var child = VisualTreeHelper.GetChild(rootObject, 0);
            if (child == null)
            {
                return null;
            }

            return child.GetType() == typeof(T) ? child : RecursiveVisualChildFinder<T>(child);
        }
        #endregion
    }
}