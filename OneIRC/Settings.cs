namespace OneIRC
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    using OneIRC.Annotations;

    public class Settings : INotifyPropertyChanged
    {
        #region Fields
        private bool expandImages;
        #endregion

        #region Constructors and Destructors
        public Settings()
        {
            this.expandImages = true;
        }
        #endregion

        #region Public Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Public Properties
        public bool ExpandImages
        {
            get { return this.expandImages; }
            set
            {
                if (value != this.expandImages)
                {
                    this.expandImages = value;
                    this.OnPropertyChanged(nameof(this.ExpandImages));
                }
            }
        }
        #endregion

        #region Methods
        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}