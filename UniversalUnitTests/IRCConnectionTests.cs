﻿// ReSharper disable AccessToDisposedClosure

namespace UniversalUnitTests
{
    #region
    using System;
    using System.Net;
    using System.Threading;

    using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

    using OneIRC.IRC;

    #endregion

    [TestClass]
    public class IRCConnectionTests
    {
        #region Public Methods and Operators
        [TestMethod]
        [TestCategory("IRC Client Basic Flow")]
        public void TestBadConnection()
        {
            var waiter = new EventWaitHandle(false, EventResetMode.ManualReset);
            var config = new IRCServerConfig("127.0.0.1");
            using (var conn = new IRCConnection(config))
            {
                var occurred = false;
                var connected = false;
                var msg = "";
                conn.ConnectionHandler += (sender, args) =>
                                              {
                                                  occurred = true;
                                                  waiter.Set();
                                                  connected = args.Status == IRCConnectionStatus.Accept_Complete;
                                                  msg = args.Message;
                                              };
                conn.Open();

                waiter.WaitOne(TimeSpan.FromSeconds(60));
                Assert.IsTrue(occurred, "Failed to wait for accept status");
                Assert.IsFalse(connected, $"Connection Failed as {connected} with message: {msg}");
            }
        }

        [TestMethod]
        [TestCategory("IRC Connection Basic Flow")]
        public void TestGoodConnection()
        {
            var waiter = new EventWaitHandle(false, EventResetMode.ManualReset);

            using (var server = new MockServer())
            {
                server.Open();

                var config = new IRCServerConfig("127.0.0.1");
                using (var conn = new IRCConnection(config))
                {
                    var occurred = false;
                    var connected = false;
                    var msg = "";
                    waiter.Reset();
                    conn.ConnectionHandler += (sender, args) =>
                                                  {
                                                      occurred = true;
                                                      waiter.Set();
                                                      connected |= args.Status == IRCConnectionStatus.Accept_Complete;
                                                      msg = args.Message;
                                                  };
                    conn.Open();

                    waiter.WaitOne(TimeSpan.FromSeconds(60));
                    Assert.IsTrue(occurred, "Failed to wait for accept status");
                    Assert.IsTrue(connected, $"Connection Failed as {connected} with message: {msg}");
                }
            }
        }

        [TestMethod]
        [TestCategory("IRC Client Basic Flow")]
        public void TestMultipleEvents()
        {
            var waiter1 = new EventWaitHandle(false, EventResetMode.ManualReset);
            var waiter2 = new EventWaitHandle(false, EventResetMode.ManualReset);
            var config = new IRCServerConfig("127.0.0.1");
            using (var conn = new IRCConnection(config))
            {
                var occurred1 = false;
                var occurred2 = false;
                conn.ConnectionHandler += (sender, args) =>
                                              {
                                                  occurred1 = true;
                                                  waiter1.Set();
                                              };
                conn.ConnectionHandler += (sender, args) =>
                                              {
                                                  occurred2 = true;
                                                  waiter2.Set();
                                              };
                conn.Open();

                waiter1.WaitOne(TimeSpan.FromSeconds(60));
                waiter2.WaitOne(TimeSpan.FromSeconds(60));
                Assert.IsTrue(occurred1, "Failed to wait for accept status");
                Assert.IsTrue(occurred2, "Failed to wait for accept status");
            }
        }

        [TestMethod]
        [TestCategory("IRC Client Basic Flow")]
        public void TestRecieveMessages()
        {
            using (var server = new MockServer())
            {
                server.ConnectionHandler += (sender, args) => { server.SendMessage(args.AcceptSocket, "Message"); };
                server.Open();

                var config = new IRCServerConfig("127.0.0.1");
                var waiter = new EventWaitHandle(false, EventResetMode.ManualReset);
                using (var conn = new IRCConnection(config))
                {
                    var occurred = false;
                    var connected = false;
                    waiter.Reset();
                    conn.ConnectionHandler +=
                        (sender, args) => { connected = args.Status == IRCConnectionStatus.Accept_Complete; };
                    conn.MessageReceivedHandler += (sender, args) =>
                                                       {
                                                           occurred = args.Status
                                                                      == IRCConnectionStatus.Message_Received;
                                                           waiter.Set();
                                                       };
                    conn.Open();

                    waiter.WaitOne(TimeSpan.FromSeconds(60));
                    Assert.IsTrue(connected, "Failed to connect");
                    Assert.IsTrue(occurred, "No messages received");
                }
            }
        }
        #endregion
    }
}