﻿namespace UniversalUnitTests
{
    using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

    using OneIRC.IRC.Messages;
    using System;

    [TestClass]
    public class ServerCommandMessageTests
    {
        #region Motd
        [TestMethod]
        [TestCategory("Motd Message")]
        [TestCategory("Server Command Message")]
        public void TestMotd()
        {
            var raw = "MOTD";
            var msg = IRCMessage.Parse(raw) as MotdMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.TargetServer, "");
        }

        [TestMethod]
        [TestCategory("Motd Message")]
        [TestCategory("Server Command Message")]
        public void TestMotdTarget()
        {
            var raw = "MOTD eff.org";
            var msg = IRCMessage.Parse(raw) as MotdMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.TargetServer, "eff.org");
        }

        [TestMethod]
        [TestCategory("Motd Message")]
        [TestCategory("Server Command Message")]
        public void TestMotdConstructor()
        {
            var msg = new MotdMessage(Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "MOTD");
        }

        [TestMethod]
        [TestCategory("Motd Message")]
        [TestCategory("Server Command Message")]
        public void TestMotdTargetConstructor()
        {
            var msg = new MotdMessage("eff.org", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "MOTD eff.org");
        }
        #endregion
    }
}
