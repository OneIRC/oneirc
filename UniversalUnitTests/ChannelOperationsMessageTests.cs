﻿namespace UniversalUnitTests
{
    #region
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

    using OneIRC.IRC.Messages;

    #endregion

    [TestClass]
    public class ChannelOperationsMessageTests
    {
        #region Join
        [TestMethod]
        [TestCategory("Join Message")]
        [TestCategory("Channel Operations Message")]
        public void TestJoinSingle()
        {
            var raw = "JOIN #foobar";
            var msg = IRCMessage.Parse(raw) as JoinMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            CollectionAssert.AreEqual(msg.Channels.ToArray(), new[] { "#foobar" });
        }

        [TestMethod]
        [TestCategory("Join Message")]
        [TestCategory("Channel Operations Message")]
        public void TestJoinSingleKey()
        {
            var raw = "JOIN &foo fubar";
            var msg = IRCMessage.Parse(raw) as JoinMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            CollectionAssert.AreEqual(msg.Channels.ToArray(), new[] { "&foo" });
            CollectionAssert.AreEqual(msg.Keys.ToArray(), new[] { "fubar" });
        }

        [TestMethod]
        [TestCategory("Join Message")]
        [TestCategory("Channel Operations Message")]
        public void TestJoinDouble1Key()
        {
            var raw = "JOIN #foo,&bar fubar";
            var msg = IRCMessage.Parse(raw) as JoinMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            CollectionAssert.AreEqual(msg.Channels.ToArray(), new[] { "#foo", "&bar" });
            CollectionAssert.AreEqual(msg.Keys.ToArray(), new[] { "fubar" });
        }

        [TestMethod]
        [TestCategory("Join Message")]
        [TestCategory("Channel Operations Message")]
        public void TestJoinDoubleKeys()
        {
            var raw = "JOIN #foo,&bar fubar,foobar";
            var msg = IRCMessage.Parse(raw) as JoinMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            CollectionAssert.AreEqual(msg.Channels.ToArray(), new[] { "#foo", "&bar" });
            CollectionAssert.AreEqual(msg.Keys.ToArray(), new[] { "fubar", "foobar" });
        }

        [TestMethod]
        [TestCategory("Join Message")]
        [TestCategory("Channel Operations Message")]
        public void TestJoinDouble()
        {
            var raw = "JOIN #foo,#bar";
            var msg = IRCMessage.Parse(raw) as JoinMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            CollectionAssert.AreEqual(msg.Channels.ToArray(), new[] { "#foo", "#bar" });
        }

        [TestMethod]
        [TestCategory("Join Message")]
        [TestCategory("Channel Operations Message")]
        public void TestJoin0()
        {
            var raw = "JOIN 0";
            var msg = IRCMessage.Parse(raw) as JoinMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            CollectionAssert.AreEqual(msg.Channels.ToArray(), new string[0]);
        }

        [TestMethod]
        [TestCategory("Join Message")]
        [TestCategory("Channel Operations Message")]
        public void TestJoinServer()
        {
            var raw = ":WiZ!jto@tolsun.oulu.fi JOIN #foo,#bar fubar,foobar";
            var msg = IRCMessage.Parse(raw) as JoinMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            CollectionAssert.AreEqual(msg.Channels.ToArray(), new[] { "#foo", "#bar" });
            CollectionAssert.AreEqual(msg.Keys.ToArray(), new[] { "fubar", "foobar" });
        }

        [TestMethod]
        [TestCategory("Join Message")]
        [TestCategory("Channel Operations Message")]
        public void TestJoinConstructorSingle()
        {
            var msg = new JoinMessage(new[] { "#foobar" }, new string[0], Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "JOIN #foobar");
        }

        [TestMethod]
        [TestCategory("Join Message")]
        [TestCategory("Channel Operations Message")]
        public void TestJoinConstructorSingleKey()
        {
            var msg = new JoinMessage(new[] { "&foo" }, new[] { "fubar" }, Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "JOIN &foo fubar");
        }

        [TestMethod]
        [TestCategory("Join Message")]
        [TestCategory("Channel Operations Message")]
        public void TestJoinConstructorDouble1Key()
        {
            var msg = new JoinMessage(new[] { "&foo", "&bar" }, new[] { "fubar" }, Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "JOIN #foo,&bar fubar");
        }

        [TestMethod]
        [TestCategory("Join Message")]
        [TestCategory("Channel Operations Message")]
        public void TestJoinConstructorDoubleKeys()
        {
            var msg = new JoinMessage(new[] { "#foo", "#bar" }, new[] { "fubar", "foobar" }, Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "JOIN #foo,#bar fubar,foobar");
        }

        [TestMethod]
        [TestCategory("Join Message")]
        [TestCategory("Channel Operations Message")]
        public void TestJoinConstructorDouble()
        {
            var msg = new JoinMessage(new[] { "#foo", "#bar" }, new string[0], Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "JOIN #foo,#bar");
        }

        [TestMethod]
        [TestCategory("Join Message")]
        [TestCategory("Channel Operations Message")]
        public void TestJoinConstructor0()
        {
            var msg = new JoinMessage(new string[0], new string[0], Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "JOIN 0");
        }
        #endregion

        #region Part
        [TestMethod]
        [TestCategory("Part Message")]
        [TestCategory("Channel Operations Message")]
        public void TestPartSingle()
        {
            var raw = "PART #twilight_zone";
            var msg = IRCMessage.Parse(raw) as PartMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            CollectionAssert.AreEqual(msg.Channels.ToArray(), new[] { "#twilight_zone" });
        }

        [TestMethod]
        [TestCategory("Part Message")]
        [TestCategory("Channel Operations Message")]
        public void TestPartDouble()
        {
            var raw = "PART #oz-ops,&group5";
            var msg = IRCMessage.Parse(raw) as PartMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            CollectionAssert.AreEqual(msg.Channels.ToArray(), new[] { "#oz-ops", "&group5" });
        }

        [TestMethod]
        [TestCategory("Part Message")]
        [TestCategory("Channel Operations Message")]
        public void TestPartServer()
        {
            var raw = ":WiZ!jto@tolsun.oulu.fi PART #playzone :I lost";
            var msg = IRCMessage.Parse(raw) as PartMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            CollectionAssert.AreEqual(msg.Channels.ToArray(), new[] { "#playzone" });
        }

        [TestMethod]
        [TestCategory("Part Message")]
        [TestCategory("Channel Operations Message")]
        public void TestPartConstructorSingleNoMessage()
        {
            var msg = new PartMessage(new[] { "#twilight_zone" }, "", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "PART #twilight_zone");
        }

        [TestMethod]
        [TestCategory("Part Message")]
        [TestCategory("Channel Operations Message")]
        public void TestPartConstructorDoubleMessage()
        {
            var msg = new PartMessage(new[] { "#oz-ops", "&group5" }, "I lost", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "PART #oz-ops,&group5 :I lost");
        }
        #endregion

        #region Mode
        private void TestMode(string raw, string channel, IEnumerable<ChannelMode> modes)
        {
            var msg = IRCMessage.Parse(raw) as ChannelModeMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.Channel, channel);
            CollectionAssert.AreEqual(msg.Modes.ToArray(), modes.ToArray());
        }

        private void TestModeConstructor(string raw, string channel, IEnumerable<ChannelMode> modes)
        {
            var msg = new ChannelModeMessage(channel, modes, Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, raw);
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeIMI()
        {
            this.TestMode(
                          "MODE #Finnish +imI *!*@*.fi",
                          "#Finnish",
                          new ChannelMode[]
                              {
                                  new InviteOnlyChannelMode(ChannelModeChangeType.Add),
                                  new ModeratedChannelMode(ChannelModeChangeType.Add),
                                  new AutoInviteChannelMode("*!*@*.fi", ChannelModeChangeType.Add)
                              });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeO()
        {
            this.TestMode(
                          "MODE #Finnish +o Kilroy",
                          "#Finnish",
                          new ChannelMode[] { new ChanOpChannelMode("Kilroy", ChannelModeChangeType.Add) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeV()
        {
            this.TestMode(
                          "MODE #Finnish +v Wiz",
                          "#Finnish",
                          new ChannelMode[] { new VoiceChannelMode("Wiz", ChannelModeChangeType.Add) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeS()
        {
            this.TestMode(
                          "MODE #Fins -s",
                          "#Fins",
                          new ChannelMode[] { new SecretChannelMode(ChannelModeChangeType.Remove) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeAddK()
        {
            this.TestMode(
                          "MODE #42 +k oulu",
                          "#42",
                          new ChannelMode[] { new KeyChannelMode("oulu", ChannelModeChangeType.Add) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeRemoveK()
        {
            this.TestMode(
                          "MODE #42 -k oulu",
                          "#42",
                          new ChannelMode[] { new KeyChannelMode("oulu", ChannelModeChangeType.Remove) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeLimit()
        {
            this.TestMode(
                          "MODE #eu-opers +l 10",
                          "#eu-opers",
                          new ChannelMode[] { new UserLimitChannelMode("10", ChannelModeChangeType.Add) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeServerLimit()
        {
            this.TestMode(
                          ":WiZ!jto@tolsun.oulu.fi MODE #eu-opers -l",
                          "#eu-opers",
                          new ChannelMode[] { new UserLimitChannelMode("", ChannelModeChangeType.Remove) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeBanMask()
        {
            this.TestMode(
                          "MODE &oulu +b",
                          "&oulu",
                          new ChannelMode[] { new BanMaskChannelMode("", ChannelModeChangeType.Add) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeSetBanMask()
        {
            this.TestMode(
                          "MODE &oulu +b *!*@*",
                          "&oulu",
                          new ChannelMode[] { new BanMaskChannelMode("*!*@*", ChannelModeChangeType.Add) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeSetBanExceptMask()
        {
            this.TestMode(
                          "MODE &oulu +b *!*@*.edu +e *!*@*.bu.edu",
                          "&oulu",
                          new ChannelMode[]
                              {
                                  new BanMaskChannelMode("*!*@*.edu", ChannelModeChangeType.Add),
                                  new ExceptionChannelMode("*!*@*.bu.edu", ChannelModeChangeType.Add)
                              });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeSetBanExceptMaskMerged()
        {
            this.TestMode(
                          "MODE #bu +be *!*@*.edu *!*@*.bu.edu",
                          "&oulu",
                          new ChannelMode[]
                              {
                                  new BanMaskChannelMode("*!*@*.edu", ChannelModeChangeType.Add),
                                  new ExceptionChannelMode("*!*@*.bu.edu", ChannelModeChangeType.Add)
                              });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeListExceptMasks()
        {
            this.TestMode(
                          "MODE #meditation e",
                          "#meditation",
                          new ChannelMode[] { new ExceptionChannelMode("", ChannelModeChangeType.None) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeListInvitationMasks()
        {
            this.TestMode(
                          "MODE #meditation I",
                          "#meditation",
                          new ChannelMode[] { new AutoInviteChannelMode("", ChannelModeChangeType.None) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeListCreator()
        {
            this.TestMode(
                          "MODE #meditation O",
                          "#meditation",
                          new ChannelMode[] { new CreatorChannelMode(ChannelModeChangeType.None) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeIMIConstructor()
        {
            this.TestMode(
                          "MODE #Finnish +imI *!*@*.fi",
                          "#Finnish",
                          new ChannelMode[]
                              {
                                  new InviteOnlyChannelMode(ChannelModeChangeType.Add),
                                  new ModeratedChannelMode(ChannelModeChangeType.Add),
                                  new AutoInviteChannelMode("*!*@*.fi", ChannelModeChangeType.Add)
                              });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeOConstructor()
        {
            this.TestMode(
                          "MODE #Finnish +o Kilroy",
                          "#Finnish",
                          new ChannelMode[] { new ChanOpChannelMode("Kilroy", ChannelModeChangeType.Add) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeVConstructor()
        {
            this.TestMode(
                          "MODE #Finnish +v Wiz",
                          "#Finnish",
                          new ChannelMode[] { new VoiceChannelMode("Wiz", ChannelModeChangeType.Add) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeSConstructor()
        {
            this.TestMode(
                          "MODE #Fins -s",
                          "#Fins",
                          new ChannelMode[] { new SecretChannelMode(ChannelModeChangeType.Remove) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeAddKConstructor()
        {
            this.TestMode(
                          "MODE #42 +k oulu",
                          "#42",
                          new ChannelMode[] { new KeyChannelMode("oulu", ChannelModeChangeType.Add) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeRemoveKConstructor()
        {
            this.TestMode(
                          "MODE #42 -k oulu",
                          "#42",
                          new ChannelMode[] { new KeyChannelMode("oulu", ChannelModeChangeType.Remove) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeLimitConstructor()
        {
            this.TestMode(
                          "MODE #eu-opers +l 10",
                          "#eu-opers",
                          new ChannelMode[] { new UserLimitChannelMode("10", ChannelModeChangeType.Add) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeServerLimitConstructor()
        {
            this.TestMode(
                          ":WiZ!jto@tolsun.oulu.fi MODE #eu-opers -l",
                          "#eu-opers",
                          new ChannelMode[] { new UserLimitChannelMode("", ChannelModeChangeType.Remove) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeBanMaskConstructor()
        {
            this.TestMode(
                          "MODE &oulu +b",
                          "&oulu",
                          new ChannelMode[] { new BanMaskChannelMode("", ChannelModeChangeType.Add) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeSetBanMaskConstructor()
        {
            this.TestMode(
                          "MODE &oulu +b *!*@*",
                          "&oulu",
                          new ChannelMode[] { new BanMaskChannelMode("*!*@*", ChannelModeChangeType.Add) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeSetBanExceptMaskConstructor()
        {
            this.TestMode(
                          "MODE &oulu +b *!*@*.edu +e *!*@*.bu.edu",
                          "&oulu",
                          new ChannelMode[]
                              {
                                  new BanMaskChannelMode("*!*@*.edu", ChannelModeChangeType.Add),
                                  new ExceptionChannelMode("*!*@*.bu.edu", ChannelModeChangeType.Add)
                              });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeSetBanExceptMaskMergedConstructor()
        {
            this.TestMode(
                          "MODE #bu +be *!*@*.edu *!*@*.bu.edu",
                          "&oulu",
                          new ChannelMode[]
                              {
                                  new BanMaskChannelMode("*!*@*.edu", ChannelModeChangeType.Add),
                                  new ExceptionChannelMode("*!*@*.bu.edu", ChannelModeChangeType.Add)
                              });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeListExceptMasksConstructor()
        {
            this.TestMode(
                          "MODE #meditation e",
                          "#meditation",
                          new ChannelMode[] { new ExceptionChannelMode("", ChannelModeChangeType.None) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeListInvitationMasksConstructor()
        {
            this.TestMode(
                          "MODE #meditation I",
                          "#meditation",
                          new ChannelMode[] { new AutoInviteChannelMode("", ChannelModeChangeType.None) });
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Channel Operations Message")]
        public void TestModeListCreatorConstructor()
        {
            this.TestMode(
                          "MODE #meditation O",
                          "#meditation",
                          new ChannelMode[] { new CreatorChannelMode(ChannelModeChangeType.None) });
        }
        #endregion

        #region Topic
        [TestMethod]
        [TestCategory("Topic Message")]
        [TestCategory("Channel Operations Message")]
        public void TestTopicServer()
        {
            var raw = ":WiZ!jto@tolsun.oulu.fi TOPIC #test :New topic";
            var msg = IRCMessage.Parse(raw) as TopicMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.Channel, "#test");
            Assert.AreEqual(msg.Topic, "New topic");
            Assert.AreEqual(msg.IsAssignment, false);
        }

        [TestMethod]
        [TestCategory("Topic Message")]
        [TestCategory("Channel Operations Message")]
        public void TestTopicSet()
        {
            var raw = "TOPIC #test :another topic";
            var msg = IRCMessage.Parse(raw) as TopicMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.Channel, "#test");
            Assert.AreEqual(msg.Topic, "another topic");
            Assert.AreEqual(msg.IsAssignment, false);
        }

        [TestMethod]
        [TestCategory("Topic Message")]
        [TestCategory("Channel Operations Message")]
        public void TestTopicClear()
        {
            var raw = "TOPIC #test :";
            var msg = IRCMessage.Parse(raw) as TopicMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.Channel, "#test");
            Assert.AreEqual(msg.Topic, "");
            Assert.AreEqual(msg.IsAssignment, false);
        }

        [TestMethod]
        [TestCategory("Topic Message")]
        [TestCategory("Channel Operations Message")]
        public void TestTopicGet()
        {
            var raw = "TOPIC #test";
            var msg = IRCMessage.Parse(raw) as TopicMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.Channel, "#test");
            Assert.AreEqual(msg.Topic, "");
            Assert.AreEqual(msg.IsAssignment, true);
        }

        [TestMethod]
        [TestCategory("Topic Message")]
        [TestCategory("Channel Operations Message")]
        public void TestTopicSetConstructor()
        {
            var msg = new TopicMessage("#test", "another topic", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "TOPIC #test :another topic");
        }

        [TestMethod]
        [TestCategory("Topic Message")]
        [TestCategory("Channel Operations Message")]
        public void TestTopicClearConstructor()
        {
            var msg = new TopicMessage("#test", "", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "TOPIC #test :");
        }

        [TestMethod]
        [TestCategory("Topic Message")]
        [TestCategory("Channel Operations Message")]
        public void TestTopicGetConstructor()
        {
            var msg = new TopicMessage("#test", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "TOPIC #test");
        }
        #endregion

        #region Names
        [TestMethod]
        [TestCategory("Names Message")]
        [TestCategory("Channel Operations Message")]
        public void TestNamesList()
        {
            var raw = "NAMES #twilight_zone,#42";
            var msg = IRCMessage.Parse(raw) as NamesMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            CollectionAssert.AreEqual(msg.Channels.ToArray(), new[] { "#twilight_zone", "#42" });
            Assert.AreEqual(msg.Target, "");
        }

        [TestMethod]
        [TestCategory("Names Message")]
        [TestCategory("Channel Operations Message")]
        public void TestNamesEmpty()
        {
            var raw = "NAMES";
            var msg = IRCMessage.Parse(raw) as NamesMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            CollectionAssert.AreEqual(msg.Channels.ToArray(), new string[0]);
            Assert.AreEqual(msg.Target, "");
        }

        [TestMethod]
        [TestCategory("Names Message")]
        [TestCategory("Channel Operations Message")]
        public void TestNamesListConstructor()
        {
            var msg = new NamesMessage(new[] { "#twilight_zone", "#42" }, Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "NAMES #twilight_zone,#42");
        }

        [TestMethod]
        [TestCategory("Names Message")]
        [TestCategory("Channel Operations Message")]
        public void TestNamesEmptyConstructor()
        {
            var msg = new NamesMessage(Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "NAMES");
        }
        #endregion

        #region List
        [TestMethod]
        [TestCategory("List Message")]
        [TestCategory("Channel Operations Message")]
        public void TestListList()
        {
            var raw = "LIST #twilight_zone,#42";
            var msg = IRCMessage.Parse(raw) as ListMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            CollectionAssert.AreEqual(msg.Channels.ToArray(), new[] { "#twilight_zone", "#42" });
            Assert.AreEqual(msg.TargetServer, "");
        }

        [TestMethod]
        [TestCategory("List Message")]
        [TestCategory("Channel Operations Message")]
        public void TestListEmpty()
        {
            var raw = "LIST";
            var msg = IRCMessage.Parse(raw) as ListMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            CollectionAssert.AreEqual(msg.Channels.ToArray(), new string[0]);
            Assert.AreEqual(msg.TargetServer, "");
        }

        [TestMethod]
        [TestCategory("List Message")]
        [TestCategory("Channel Operations Message")]
        public void TestListListConstructor()
        {
            var msg = new ListMessage(new[] { "#twilight_zone", "#42" }, Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "LIST #twilight_zone,#42");
        }

        [TestMethod]
        [TestCategory("List Message")]
        [TestCategory("Channel Operations Message")]
        public void TestListEmptyConstructor()
        {
            var msg = new ListMessage(Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "LIST");
        }
        #endregion

        #region Invite
        [TestMethod]
        [TestCategory("Invite Message")]
        [TestCategory("Channel Operations Message")]
        public void TestInviteServer()
        {
            var raw = ":Angel!wings@irc.org INVITE Wiz #Dust";
            var msg = IRCMessage.Parse(raw) as InviteMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.NickName, "Angel");
            Assert.AreEqual(msg.TargetNickName, "Wiz");
            Assert.AreEqual(msg.Channel, "#Dust");
        }

        [TestMethod]
        [TestCategory("Invite Message")]
        [TestCategory("Channel Operations Message")]
        public void TestInviteClient()
        {
            var raw = "INVITE Wiz #Twilight_Zone";
            var msg = IRCMessage.Parse(raw) as InviteMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.TargetNickName, "Wiz");
            Assert.AreEqual(msg.Channel, "#Twilight_Zone");
        }

        [TestMethod]
        [TestCategory("Invite Message")]
        [TestCategory("Channel Operations Message")]
        public void TestInviteConstructor()
        {
            var msg = new InviteMessage("Wiz", "#Twilight_Zone", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "INVITE Wiz #Twilight_Zone");
        }
        #endregion

        #region Kick
        [TestMethod]
        [TestCategory("Kick Message")]
        [TestCategory("Channel Operations Message")]
        public void TestKickSimple()
        {
            var raw = "KICK &Melbourne Matthew";
            var msg = IRCMessage.Parse(raw) as KickMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            CollectionAssert.AreEqual(msg.Channels.ToArray(), new[] { "&Melbourne" });
            CollectionAssert.AreEqual(msg.TargetNickNames.ToArray(), new[] { "Matthew" });
            Assert.AreEqual(msg.Comment, "");
        }

        [TestMethod]
        [TestCategory("Kick Message")]
        [TestCategory("Channel Operations Message")]
        public void TestKickComment()
        {
            var raw = "KICK #Finnish John :Speaking English";
            var msg = IRCMessage.Parse(raw) as KickMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            CollectionAssert.AreEqual(msg.Channels.ToArray(), new[] { "#Finnish" });
            CollectionAssert.AreEqual(msg.TargetNickNames.ToArray(), new[] { "John" });
            Assert.AreEqual(msg.Comment, "Speaking English");
        }

        [TestMethod]
        [TestCategory("Kick Message")]
        [TestCategory("Channel Operations Message")]
        public void TestKickServer()
        {
            var raw = ":WiZ!jto@tolsun.oulu.fi KICK #Finnish John";
            var msg = IRCMessage.Parse(raw) as KickMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.NickName, "WiZ");
            CollectionAssert.AreEqual(msg.Channels.ToArray(), new[] { "#Finnish" });
            CollectionAssert.AreEqual(msg.TargetNickNames.ToArray(), new[] { "John" });
        }

        [TestMethod]
        [TestCategory("Kick Message")]
        [TestCategory("Channel Operations Message")]
        public void TestKickSimpleConstructor()
        {
            var msg = new KickMessage("&Melbourne", "Matthew", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "KICK &Melbourne Matthew");
        }

        [TestMethod]
        [TestCategory("Kick Message")]
        [TestCategory("Channel Operations Message")]
        public void TestKickCommentConstructor()
        {
            var msg = new KickMessage("#Finnish", "John", "Speaking English", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "KICK #Finnish John :Speaking English");
        }
        #endregion
    }
}