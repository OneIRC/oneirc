﻿namespace UniversalUnitTests
{
    #region
    using System;
    using System.Linq;

    using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

    using OneIRC.IRC.Messages;

    #endregion

    [TestClass]
    public class UserQueryMessageTests
    {
        #region Who
        [TestMethod]
        [TestCategory("Who Message")]
        [TestCategory("User Query Message")]
        public void TestWho()
        {
            var raw = "WHO *.fi";
            var msg = IRCMessage.Parse(raw) as WhoMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.Mask, "*.fi");
            Assert.AreEqual(msg.OperatorsOnly, false);
        }

        [TestMethod]
        [TestCategory("Who Message")]
        [TestCategory("User Query Message")]
        public void TestWhoO()
        {
            var raw = "WHO jto* o";
            var msg = IRCMessage.Parse(raw) as WhoMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.Mask, "jto*");
            Assert.AreEqual(msg.OperatorsOnly, true);
        }

        [TestMethod]
        [TestCategory("Who Message")]
        [TestCategory("User Query Message")]
        public void TestWhoConstructor()
        {
            var msg = new WhoMessage("*.fi", false, Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "WHO *.fi");
        }

        [TestMethod]
        [TestCategory("Who Message")]
        [TestCategory("User Query Message")]
        public void TestWhoOConstructor()
        {
            var msg = new WhoMessage("jto*", true, Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "WHO jto* o");
        }
        #endregion

        #region Whois
        [TestMethod]
        [TestCategory("Whois Message")]
        [TestCategory("User Query Message")]
        public void TestWhois()
        {
            var raw = "WHOIS wiz";
            var msg = IRCMessage.Parse(raw) as WhoisMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            CollectionAssert.AreEqual(msg.Masks.ToArray(), new[] { "wiz" });
            Assert.AreEqual(msg.Target, "");
        }

        [TestMethod]
        [TestCategory("Whois Message")]
        [TestCategory("User Query Message")]
        public void TestWhoisTarget()
        {
            var raw = "WHOIS eff.org trillian";
            var msg = IRCMessage.Parse(raw) as WhoisMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            CollectionAssert.AreEqual(msg.Masks.ToArray(), new[] { "trillian" });
            Assert.AreEqual(msg.Target, "eff.org");
        }

        [TestMethod]
        [TestCategory("Whois Message")]
        [TestCategory("User Query Message")]
        public void TestWhoisConstructor()
        {
            var msg = new WhoisMessage("wiz", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "WHOIS wiz");
        }

        [TestMethod]
        [TestCategory("Whois Message")]
        [TestCategory("User Query Message")]
        public void TestWhoisTargetConstructor()
        {
            var msg = new WhoisMessage("trillian", "eff.org", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "WHOIS eff.org trillian");
        }
        #endregion

        #region Whowas
        [TestMethod]
        [TestCategory("WhoWas Message")]
        [TestCategory("User Query Message")]
        public void TestWhoWas()
        {
            var raw = "WHOWAS Wiz";
            var msg = IRCMessage.Parse(raw) as WhoWasMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            CollectionAssert.AreEqual(msg.Nicks.ToArray(), new[] { "Wiz" });
            Assert.AreEqual(msg.Count, -1);
            Assert.AreEqual(msg.Target, "");
        }

        [TestMethod]
        [TestCategory("WhoWas Message")]
        [TestCategory("User Query Message")]
        public void TestWhoWasCount()
        {
            var raw = "WHOWAS Mermaid 9";
            var msg = IRCMessage.Parse(raw) as WhoWasMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            CollectionAssert.AreEqual(msg.Nicks.ToArray(), new[] { "Mermaid" });
            Assert.AreEqual(msg.Count, 9);
            Assert.AreEqual(msg.Target, "");
        }

        [TestMethod]
        [TestCategory("WhoWas Message")]
        [TestCategory("User Query Message")]
        public void TestWhoWasTarget()
        {
            var raw = "WHOWAS Trillian 1 *.edu";
            var msg = IRCMessage.Parse(raw) as WhoWasMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            CollectionAssert.AreEqual(msg.Nicks.ToArray(), new[] { "Trillian" });
            Assert.AreEqual(msg.Count, 1);
            Assert.AreEqual(msg.Target, "*.edu");
        }

        [TestMethod]
        [TestCategory("WhoWas Message")]
        [TestCategory("User Query Message")]
        public void TestWhoWasConstructor()
        {
            var msg = new WhoWasMessage("Wiz", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "WHOWAS Wiz");
        }

        [TestMethod]
        [TestCategory("WhoWas Message")]
        [TestCategory("User Query Message")]
        public void TestWhoWasCountConstructor()
        {
            var msg = new WhoWasMessage("Mermaid", 9, Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "WHOWAS Mermaid 9");
        }

        [TestMethod]
        [TestCategory("WhoWas Message")]
        [TestCategory("User Query Message")]
        public void TestWhoWasTargetConstructor()
        {
            var msg = new WhoWasMessage("Trillian", 1, "*.edu", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "WHOWAS Trillian 1 *.edu");
        }
        #endregion
    }
}