﻿namespace UniversalUnitTests
{
    #region
    using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

    using OneIRC.IRC.Messages;

    #endregion

    [TestClass]
    public class SendingClassMessageTests
    {
        #region PrivMsg
        [TestMethod]
        [TestCategory("PrivMsg Message")]
        [TestCategory("Sending Class Message")]
        public void TestPrivMsgServerMessage()
        {
            var raw = ":Angel!wings@irc.org PRIVMSG Wiz :Are you receiving this message ?";
            var msg = IRCMessage.Parse(raw) as PrivMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.NickName, "Angel");
            Assert.AreEqual(msg.Target, "Wiz");
            Assert.AreEqual(msg.Message, "Are you receiving this message ?");
        }

        [TestMethod]
        [TestCategory("PrivMsg Message")]
        [TestCategory("Sending Class Message")]
        public void TestPrivMsgClientSimple()
        {
            var raw = "PRIVMSG Angel :yes I'm receiving it !";
            var msg = IRCMessage.Parse(raw) as PrivMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.Target, "Angel");
            Assert.AreEqual(msg.Message, "yes I'm receiving it !");
        }

        [TestMethod]
        [TestCategory("PrivMsg Message")]
        [TestCategory("Sending Class Message")]
        public void TestPrivMsgClientToServer()
        {
            var raw = "PRIVMSG $*.fi :Server tolsun.oulu.fi rebooting.";
            var msg = IRCMessage.Parse(raw) as PrivMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.Target, "$*.fi");
            Assert.AreEqual(msg.Message, "Server tolsun.oulu.fi rebooting.");
        }

        [TestMethod]
        [TestCategory("PrivMsg Message")]
        [TestCategory("Sending Class Message")]
        public void TestPrivMsgClientToChannel()
        {
            var raw = "PRIVMSG #finnish :This is a test";
            var msg = IRCMessage.Parse(raw) as PrivMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.Target, "#finnish");
            Assert.AreEqual(msg.Message, "This is a test");
        }

        [TestMethod]
        [TestCategory("PrivMsg Message")]
        [TestCategory("Sending Class Message")]
        public void TestPrivMsgConstructor()
        {
            var msg = new PrivMessage("Angel", "yes I'm receiving it !");

            Assert.AreEqual(msg.RawMessage, "PRIVMSG Angel :yes I'm receiving it !");
        }

        [TestMethod]
        [TestCategory("PrivMsg Message")]
        [TestCategory("Sending Class Message")]
        public void TestPrivMsgRoomConstructor()
        {
            var msg = new PrivMessage("#finnish", "This is a test");

            Assert.AreEqual(msg.RawMessage, "PRIVMSG #finnish :This is a test");
        }
        #endregion

        #region Notice
        [TestMethod]
        [TestCategory("Notice Message")]
        [TestCategory("Sending Class Message")]
        public void TestNoticeServerMessage()
        {
            var raw = ":Angel!wings@irc.org NOTICE Wiz :Are you receiving this message ?";
            var msg = IRCMessage.Parse(raw) as NoticeMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.NickName, "Angel");
            Assert.AreEqual(msg.Target, "Wiz");
            Assert.AreEqual(msg.Message, "Are you receiving this message ?");
        }

        [TestMethod]
        [TestCategory("Notice Message")]
        [TestCategory("Sending Class Message")]
        public void TestNoticeClientSimple()
        {
            var raw = "NOTICE Angel :yes I'm receiving it !";
            var msg = IRCMessage.Parse(raw) as NoticeMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.Target, "Angel");
            Assert.AreEqual(msg.Message, "yes I'm receiving it !");
        }

        [TestMethod]
        [TestCategory("Notice Message")]
        [TestCategory("Sending Class Message")]
        public void TestNoticeClientToServer()
        {
            var raw = "NOTICE $*.fi :Server tolsun.oulu.fi rebooting.";
            var msg = IRCMessage.Parse(raw) as NoticeMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.Target, "$*.fi");
            Assert.AreEqual(msg.Message, "Server tolsun.oulu.fi rebooting.");
        }

        [TestMethod]
        [TestCategory("Notice Message")]
        [TestCategory("Sending Class Message")]
        public void TestNoticeClientToChannel()
        {
            var raw = "NOTICE #finnish :This is a test";
            var msg = IRCMessage.Parse(raw) as NoticeMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.Target, "#finnish");
            Assert.AreEqual(msg.Message, "This is a test");
        }

        [TestMethod]
        [TestCategory("Notice Message")]
        [TestCategory("Sending Class Message")]
        public void TestNoticeConstructor()
        {
            var msg = new NoticeMessage("Angel", "yes I'm receiving it !");

            Assert.AreEqual(msg.RawMessage, "NOTICE Angel :yes I'm receiving it !");
        }

        [TestMethod]
        [TestCategory("Notice Message")]
        [TestCategory("Sending Class Message")]
        public void TestNoticeRoomConstructor()
        {
            var msg = new NoticeMessage("#finnish", "This is a test");

            Assert.AreEqual(msg.RawMessage, "NOTICE #finnish :This is a test");
        }
        #endregion
    }
}