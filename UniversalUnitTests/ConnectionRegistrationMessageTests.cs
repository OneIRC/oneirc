﻿namespace UniversalUnitTests
{
    #region
    using System;

    using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

    using OneIRC.IRC.Messages;

    #endregion

    [TestClass]
    public class ConnectionRegistrationMessageTests
    {
        #region Pass
        [TestMethod]
        [TestCategory("Pass Message")]
        [TestCategory("Connection Registration Message")]
        public void TestPass()
        {
            var raw = "PASS secretpasswordhere";
            var msg = IRCMessage.Parse(raw) as PassMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.Pass, "secretpasswordhere");
        }

        [TestMethod]
        [TestCategory("Pass Message")]
        [TestCategory("Connection Registration Message")]
        public void TestPassConstructor()
        {
            var msg = new PassMessage("secretpasswordhere", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "PASS secretpasswordhere");
        }
        #endregion

        #region Nick
        [TestMethod]
        [TestCategory("Nick Message")]
        [TestCategory("Connection Registration Message")]
        public void TestNickClient()
        {
            var raw = "NICK Wiz";
            var msg = IRCMessage.Parse(raw) as NickMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.NewNickName, "Wiz");
        }

        [TestMethod]
        [TestCategory("Nick Message")]
        [TestCategory("Connection Registration Message")]
        public void TestNickServer()
        {
            var raw = ":WiZ!jto@tolsun.oulu.fi NICK Kilroy";
            var msg = IRCMessage.Parse(raw) as NickMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.NickName, "WiZ");
            Assert.AreEqual(msg.NewNickName, "Kilroy");
        }

        [TestMethod]
        [TestCategory("Nick Message")]
        [TestCategory("Connection Registration Message")]
        public void TestNickConstructor()
        {
            var msg = new NickMessage("Wiz", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "NICK Wiz");
        }
        #endregion

        #region User
        [TestMethod]
        [TestCategory("User Message")]
        [TestCategory("Connection Registration Message")]
        public void TestUser0()
        {
            var raw = "USER guest 0 * :Ronnie Reagan";
            var msg = IRCMessage.Parse(raw) as UserMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.NewUserName, "guest");
            Assert.AreEqual(msg.Mode, 0);
            Assert.AreEqual(msg.RealName, "Ronnie Reagan");
        }

        [TestMethod]
        [TestCategory("User Message")]
        [TestCategory("Connection Registration Message")]
        public void TestUser8()
        {
            var raw = "USER guest 8 * :Ronnie Reagan";
            var msg = IRCMessage.Parse(raw) as UserMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.NewUserName, "guest");
            Assert.AreEqual(msg.Mode, 8);
            Assert.AreEqual(msg.RealName, "Ronnie Reagan");
        }

        [TestMethod]
        [TestCategory("User Message")]
        [TestCategory("Connection Registration Message")]
        public void TestUser0Constructor()
        {
            var msg = new UserMessage("guest", 0, "Ronnie Reagan", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "USER guest 0 * :Ronnie Reagan");
        }

        [TestMethod]
        [TestCategory("User Message")]
        [TestCategory("Connection Registration Message")]
        public void TestUser8Constructor()
        {
            var msg = new UserMessage("guest", 8, "Ronnie Reagan", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "USER guest 8 * :Ronnie Reagan");
        }
        #endregion

        #region Oper
        [TestMethod]
        [TestCategory("Oper Message")]
        [TestCategory("Connection Registration Message")]
        public void TestOper()
        {
            var raw = "OPER foo bar";
            var msg = IRCMessage.Parse(raw) as OperMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.Name, "foo");
            Assert.AreEqual(msg.Pass, "bar");
        }

        [TestMethod]
        [TestCategory("Oper Message")]
        [TestCategory("Connection Registration Message")]
        public void TestOperConstructor()
        {
            var msg = new OperMessage("foo", "bar", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "OPER foo bar");
        }
        #endregion

        #region Mode
        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Connection Registration Message")]
        public void TestModeRemoveW()
        {
            var raw = "MODE WiZ -w";
            var msg = IRCMessage.Parse(raw) as UserModeMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.NickName, "WiZ");
            Assert.AreEqual(msg.Type, UserModeChangeType.Remove);
            Assert.AreEqual(msg.Mode, UserMode.Wallops);
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Connection Registration Message")]
        public void TestModeAddI()
        {
            var raw = "MODE Angel +i";
            var msg = IRCMessage.Parse(raw) as UserModeMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.NickName, "Angel");
            Assert.AreEqual(msg.Type, UserModeChangeType.Add);
            Assert.AreEqual(msg.Mode, UserMode.Invisible);
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Connection Registration Message")]
        public void TestModeRemoveLowerO()
        {
            var raw = "MODE WiZ -o";
            var msg = IRCMessage.Parse(raw) as UserModeMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.NickName, "WiZ");
            Assert.AreEqual(msg.Type, UserModeChangeType.Remove);
            Assert.AreEqual(msg.Mode, UserMode.Operator);
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Connection Registration Message")]
        public void TestModeRemoveUpperO()
        {
            var raw = "MODE WiZ -O";
            var msg = IRCMessage.Parse(raw) as UserModeMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.NickName, "WiZ");
            Assert.AreEqual(msg.Type, UserModeChangeType.Remove);
            Assert.AreEqual(msg.Mode, UserMode.LocalOperator);
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Connection Registration Message")]
        public void TestModeConstructorAddS()
        {
            var msg = new UserModeMessage("Angel", UserMode.Invisible, UserModeChangeType.Add, Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "MODE Angel +i");
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Connection Registration Message")]
        public void TestModeConstructorRemoveLowerO()
        {
            var msg = new UserModeMessage("WiZ", UserMode.Operator, UserModeChangeType.Remove, Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "MODE WiZ -o");
        }

        [TestMethod]
        [TestCategory("Mode Message")]
        [TestCategory("Connection Registration Message")]
        public void TestModeConstructorRemoveUpperO()
        {
            var msg = new UserModeMessage("WiZ", UserMode.LocalOperator, UserModeChangeType.Remove, Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "MODE WiZ -O");
        }
        #endregion

        #region Service
        [TestMethod]
        [TestCategory("Service Message")]
        [TestCategory("Connection Registration Message")]
        public void TestService()
        {
            var raw = "SERVICE dict * *.fr 0 0 :French Dictionary";
            var msg = IRCMessage.Parse(raw) as ServiceMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.NewNick, "dict");
            Assert.AreEqual(msg.Distribution, "*.fr");
            Assert.AreEqual(msg.Info, "French Dictionary");
        }

        [TestMethod]
        [TestCategory("Service Message")]
        [TestCategory("Connection Registration Message")]
        public void TestServiceConstructor()
        {
            var msg = new ServiceMessage("dict", "*.fr", "French Dictionary", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "SERVICE dict * *.fr 0 0 :French Dictionary");
        }
        #endregion

        #region Quit
        [TestMethod]
        [TestCategory("Quit Message")]
        [TestCategory("Connection Registration Message")]
        public void TestQuitClient()
        {
            var raw = "QUIT :Gone to have lunch";
            var msg = IRCMessage.Parse(raw) as QuitMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.Message, "Gone to have lunch");
        }

        [TestMethod]
        [TestCategory("Quit Message")]
        [TestCategory("Connection Registration Message")]
        public void TestQuitServer()
        {
            var raw = ":syrk!kalt@millennium.stealth.net QUIT :Gone to have lunch";
            var msg = IRCMessage.Parse(raw) as QuitMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.NickName, "syrk");
            Assert.AreEqual(msg.Message, "Gone to have lunch");
        }

        [TestMethod]
        [TestCategory("Quit Message")]
        [TestCategory("Connection Registration Message")]
        public void TestQuitConstructor()
        {
            var msg = new QuitMessage("Gone to have lunch", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "QUIT :Gone to have lunch");
        }
        #endregion

        #region SQuit
        [TestMethod]
        [TestCategory("SQuit Message")]
        [TestCategory("Connection Registration Message")]
        public void TestSQuitClient()
        {
            var raw = "SQUIT tolsun.oulu.fi :Bad Link ?";
            var msg = IRCMessage.Parse(raw) as SQuitMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.TargetServer, "tolsun.oulu.fi");
            Assert.AreEqual(msg.Message, "Bad Link ?");
        }

        [TestMethod]
        [TestCategory("SQuit Message")]
        [TestCategory("Connection Registration Message")]
        public void TestSQuitServer()
        {
            var raw = ":Trillian SQUIT cm22.eng.umd.edu :Server out of control";
            var msg = IRCMessage.Parse(raw) as SQuitMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.TargetServer, "cm22.eng.umd.edu");
            Assert.AreEqual(msg.Message, "Server out of control");
        }

        [TestMethod]
        [TestCategory("SQuit Message")]
        [TestCategory("Connection Registration Message")]
        public void TestSQuitConstructor()
        {
            var msg = new SQuitMessage("tolsun.oulu.fi", "Bad Link ?", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "SQUIT tolsun.oulu.fi :Bad Link ?");
        }
        #endregion
    }
}