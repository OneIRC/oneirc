﻿// ReSharper disable AccessToDisposedClosure

namespace UniversalUnitTests
{
    #region
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading;

    using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

    using OneIRC.IRC;
    using OneIRC.IRC.Messages;

    #endregion

    [TestClass]
    public class IRCClientBasicTests
    {
        #region Properties
        protected static IEnumerable<string> ConnectedMessages
            =>
                new List<string>
                    {
                        ":irc.localhost NOTICE * :*** Looking up stuff",
                        ":irc.localhost NOTICE * :*** Doing other stuff"
                    };
        #endregion

        #region Public Methods and Operators
        [TestMethod]
        [TestCategory("IRC Client Basic Flow")]
        public void TestPingPong()
        {
            var expectedStartup = new List<Type>
                                      {
                                          typeof(PassMessage),
                                          typeof(NickMessage),
                                          typeof(UserMessage),
                                          typeof(PongMessage)
                                      };
            this.TestRecievedFlow(
                                  expectedStartup,
                                  (sender, args) =>
                                      {
                                          if (IRCMessage.Parse(args.Message) is UserMessage)
                                          {
                                              args.Server.SendMessage(
                                                                      args.Socket,
                                                                      new PingMessage("Test Message").RawMessage);
                                          }
                                      });
        }

        public void TestRecievedFlow(IEnumerable<Type> expected, EventHandler<MockArgs> advancedMessageHandler = null)
        {
            var waiter =
                expected.Select(
                                x =>
                                new ServerQueue
                                    {
                                        Waiter = new EventWaitHandle(false, EventResetMode.ManualReset),
                                        Occured = false,
                                        Type = x
                                    }).ToList();
            var outOfOrder = false;

            using (var server = new MockServer())
            {
                server.ConnectionHandler += (sender, args) =>
                                                {
                                                    if (args.LastOperation == SocketAsyncOperation.Accept)
                                                    {
                                                        foreach (var msg in ConnectedMessages)
                                                        {
                                                            server.SendMessage(args.AcceptSocket, msg);
                                                        }
                                                    }
                                                };
                server.MessageReceivedHandler += (sender, args) =>
                                                     {
                                                         lock (waiter)
                                                         {
                                                             var next = waiter.First(s => s.Occured == false);
                                                             if (next != null
                                                                 && IRCMessage.Parse(args).GetType() == next.Type)
                                                             {
                                                                 next.Occured = true;
                                                                 next.Waiter.Set();
                                                             }
                                                             else
                                                             {
                                                                 outOfOrder = true;
                                                             }
                                                         }
                                                     };
                if (advancedMessageHandler != null)
                {
                    server.AdvancedMessageHandler += advancedMessageHandler;
                }
                server.Open();

                var sconfig = new IRCServerConfig("127.0.0.1");
                var cconfig = new IRCClientConfig(sconfig, "testnick", "test user");
                using (var conn = new IRCClient(cconfig, SynchronizationContext.Current))
                {
                    lock (waiter)
                    {
                        foreach (var wait in waiter)
                        {
                            wait.Waiter.Reset();
                        }
                    }
                    conn.Open();

                    foreach (var wait in waiter)
                    {
                        wait.Waiter.WaitOne(TimeSpan.FromSeconds(10));
                    }
                }
            }

            foreach (var wait in waiter)
            {
                Assert.IsTrue(wait.Occured, $"Failed to receive {wait.Type}");
            }
            Assert.IsFalse(outOfOrder, "Messages were received out of order");
        }

        [TestMethod]
        [TestCategory("IRC Client Basic Flow")]
        public void TestStartupFlow()
        {
            var expectedStartup = new List<Type> { typeof(PassMessage), typeof(NickMessage), typeof(UserMessage) };
            this.TestRecievedFlow(expectedStartup);
        }
        #endregion

        internal class ServerQueue
        {
            #region Public Properties
            public bool Occured { get; set; }
            public Type Type { get; set; }
            public EventWaitHandle Waiter { get; set; }
            #endregion
        }
    }
}