﻿namespace UniversalUnitTests
{
    #region
    using System;

    using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

    using OneIRC.IRC.Messages;

    #endregion

    [TestClass]
    public class MiscellaneousMessageTests
    {
        #region Kill
        [TestMethod]
        [TestCategory("Kill Message")]
        [TestCategory("Miscellaneous Message")]
        public void TestKill()
        {
            var raw = "KILL :wiz Speaking English";
            var msg = IRCMessage.Parse(raw) as KillMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.TargetNickName, "wiz");
            Assert.AreEqual(msg.Comment, "Speaking English");
        }

        [TestMethod]
        [TestCategory("Kill Message")]
        [TestCategory("Miscellaneous Message")]
        public void TestKillConstructor()
        {
            var msg = new KillMessage("wiz", "Speaking English", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "KILL :wiz Speaking English");
        }
        #endregion

        #region Ping
        [TestMethod]
        [TestCategory("Ping Message")]
        [TestCategory("Miscellaneous Message")]
        public void TestPing()
        {
            var raw = "PING :irc.funet.fi";
            var msg = IRCMessage.Parse(raw) as PingMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.Message, "irc.funet.fi");
        }

        [TestMethod]
        [TestCategory("Ping Message")]
        [TestCategory("Miscellaneous Message")]
        public void TestPingConstructor()
        {
            var msg = new PingMessage("irc.funet.fi");

            Assert.AreEqual(msg.RawMessage, "PING :irc.funet.fi");
        }
        #endregion

        #region Pong
        [TestMethod]
        [TestCategory("Pong Message")]
        [TestCategory("Miscellaneous Message")]
        public void TestPong()
        {
            var raw = "PONG :irc.funet.fi";
            var msg = IRCMessage.Parse(raw) as PongMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.Message, "irc.funet.fi");
        }

        [TestMethod]
        [TestCategory("Pong Message")]
        [TestCategory("Miscellaneous Message")]
        public void TestPongConstructor()
        {
            var msg = new PongMessage("irc.funet.fi");

            Assert.AreEqual(msg.RawMessage, "PONG :irc.funet.fi");
        }
        #endregion

        #region Error
        [TestMethod]
        [TestCategory("Error Message")]
        [TestCategory("Miscellaneous Message")]
        public void TestError()
        {
            var raw = "ERROR :Server *.fi already exists";
            var msg = IRCMessage.Parse(raw) as ErrorMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.Message, "Server *.fi already exists");
        }

        [TestMethod]
        [TestCategory("Error Message")]
        [TestCategory("Miscellaneous Message")]
        public void TestErrorConstructor()
        {
            var msg = new ErrorMessage("Server *.fi already exists", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "ERROR :Server *.fi already exists");
        }
        #endregion
    }
}