﻿namespace UniversalUnitTests
{
    #region
    using System;

    using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

    using OneIRC.IRC.Messages;

    #endregion

    [TestClass]
    public class OptionalMessageTests
    {
        #region Away
        [TestMethod]
        [TestCategory("Away Message")]
        [TestCategory("Optional Message")]
        public void TestAway()
        {
            var raw = "AWAY :Gone to lunch.  Back in 5";
            var msg = IRCMessage.Parse(raw) as AwayMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.Message, "Gone to lunch.  Back in 5");
        }

        [TestMethod]
        [TestCategory("Away Message")]
        [TestCategory("Optional Message")]
        public void TestAwayClear()
        {
            var raw = "AWAY";
            var msg = IRCMessage.Parse(raw) as AwayMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.Message, "");
        }

        [TestMethod]
        [TestCategory("Away Message")]
        [TestCategory("Optional Message")]
        public void TestAwayConstructor()
        {
            var msg = new AwayMessage("Gone to lunch.  Back in 5", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "AWAY :Gone to lunch.  Back in 5");
        }

        [TestMethod]
        [TestCategory("Away Message")]
        [TestCategory("Optional Message")]
        public void TestAwayClearConstructor()
        {
            var msg = new AwayMessage(Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "AWAY");
        }
        #endregion

        #region Rehash
        [TestMethod]
        [TestCategory("Rehash Message")]
        [TestCategory("Optional Message")]
        public void TestRehash()
        {
            var raw = "REHASH";
            var msg = IRCMessage.Parse(raw) as RehashMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
        }

        [TestMethod]
        [TestCategory("Rehash Message")]
        [TestCategory("Optional Message")]
        public void TestRehashConstructor()
        {
            var msg = new RehashMessage(Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "REHASH");
        }
        #endregion

        #region Die
        [TestMethod]
        [TestCategory("Die Message")]
        [TestCategory("Optional Message")]
        public void TestDie()
        {
            var raw = "DIE";
            var msg = IRCMessage.Parse(raw) as DieMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
        }

        [TestMethod]
        [TestCategory("Die Message")]
        [TestCategory("Optional Message")]
        public void TestDieConstructor()
        {
            var msg = new DieMessage(Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "DIE");
        }
        #endregion

        #region Restart
        [TestMethod]
        [TestCategory("Restart Message")]
        [TestCategory("Optional Message")]
        public void TestRestart()
        {
            var raw = "RESTART";
            var msg = IRCMessage.Parse(raw) as RestartMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
        }

        [TestMethod]
        [TestCategory("Restart Message")]
        [TestCategory("Optional Message")]
        public void TestRestartConstructor()
        {
            var msg = new RestartMessage(Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "RESTART");
        }
        #endregion

        #region Summon
        [TestMethod]
        [TestCategory("Summon Message")]
        [TestCategory("Optional Message")]
        public void TestSummon()
        {
            var raw = "SUMMON jto";
            var msg = IRCMessage.Parse(raw) as SummonMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.User, "jto");
        }

        [TestMethod]
        [TestCategory("Summon Message")]
        [TestCategory("Optional Message")]
        public void TestSummonServer()
        {
            var raw = "SUMMON jto tolsun.oulu.fi";
            var msg = IRCMessage.Parse(raw) as SummonMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.User, "jto");
            Assert.AreEqual(msg.TargetServer, "tolsun.oulu.fi");
        }

        [TestMethod]
        [TestCategory("Summon Message")]
        [TestCategory("Optional Message")]
        public void TestSummonConstructor()
        {
            var msg = new SummonMessage("jto", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "SUMMON jto");
        }

        [TestMethod]
        [TestCategory("Summon Message")]
        [TestCategory("Optional Message")]
        public void TestSummonServerConstructor()
        {
            var msg = new SummonMessage("jto", "tolsun.oulu.fi", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "SUMMON jto tolsun.oulu.fi");
        }
        #endregion

        #region Users
        [TestMethod]
        [TestCategory("Users Message")]
        [TestCategory("Optional Message")]
        public void TestUsers()
        {
            var raw = "USERS eff.org";
            var msg = IRCMessage.Parse(raw) as UsersMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.TargetServer, "eff.org");
        }

        [TestMethod]
        [TestCategory("Users Message")]
        [TestCategory("Optional Message")]
        public void TestUsersConstructor()
        {
            var msg = new UsersMessage("eff.org", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "USERS eff.org");
        }
        #endregion

        #region Operwall
        [TestMethod]
        [TestCategory("Operwall Message")]
        [TestCategory("Optional Message")]
        public void TestOperwall()
        {
            var raw = ":csd.bu.edu WALLOPS :Connect '*.uiuc.edu 6667' from Joshua";
            var msg = IRCMessage.Parse(raw) as OperwallMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.Message, "Connect '*.uiuc.edu 6667' from Joshua");
        }

        [TestMethod]
        [TestCategory("Operwall Message")]
        [TestCategory("Optional Message")]
        public void TestOperwallConstructor()
        {
            var msg = new OperwallMessage("Connect '*.uiuc.edu 6667' from Joshua", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "WALLOPS :Connect '*.uiuc.edu 6667' from Joshua");
        }
        #endregion

        #region Userhost
        [TestMethod]
        [TestCategory("Userhost Message")]
        [TestCategory("Optional Message")]
        public void TestUserhost()
        {
            var raw = "USERHOST Wiz Michael syrk";
            var msg = IRCMessage.Parse(raw) as UserhostMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(msg.TargetNicknames, new[] { "Wiz", "Michael", "syrk" });
        }

        [TestMethod]
        [TestCategory("Userhost Message")]
        [TestCategory("Optional Message")]
        public void TestUserhostConstructor()
        {
            var msg = new UserhostMessage(new[] { "Wiz", "Michael", "syrk" }, Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "USERHOST Wiz Michael syrk");
        }
        #endregion

        #region IsOn
        [TestMethod]
        [TestCategory("IsOn Message")]
        [TestCategory("Optional Message")]
        public void TestIsOn()
        {
            var raw = "ISON phone trillian WiZ jarlek Avalon Angel Monstah syrk";
            var msg = IRCMessage.Parse(raw) as IsOnMessage;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.RawMessage, raw);
            Assert.AreEqual(
                            msg.TargetNicknames,
                            new[] { "phone", "trillian", "WiZ", "jarlek", "Avalon", "Angel", "Monstah", "syrk" });
        }

        [TestMethod]
        [TestCategory("IsOn Message")]
        [TestCategory("Optional Message")]
        public void TestIsonConstructor()
        {
            var msg =
                new IsOnMessage(
                    new[] { "phone", "trillian", "WiZ", "jarlek", "Avalon", "Angel", "Monstah", "syrk" },
                    Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "ISON phone trillian WiZ jarlek Avalon Angel Monstah syrk");
        }
        #endregion
    }
}