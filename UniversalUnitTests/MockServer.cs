﻿namespace UniversalUnitTests
{
    #region
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;

    #endregion

    public class MockArgs
    {
        #region Public Properties
        public string Message { get; set; }
        public MockServer Server { get; set; }
        public Socket Socket { get; set; }
        #endregion
    }

    public class MockServer : IDisposable
    {
        #region Constructors and Destructors
        public MockServer(int port = 6667)
        {
            this.Port = port;
        }
        #endregion

        #region Public Events
        public event EventHandler<MockArgs> AdvancedMessageHandler;
        public event EventHandler<SocketAsyncEventArgs> ConnectionHandler;
        public event EventHandler<string> MessageReceivedHandler;
        #endregion

        #region Properties
        protected static int Backlog => 10;

        protected static int BufferSize => 512;
        protected string CurrentInput { get; set; }
        protected int Port { get; set; }

        protected Socket Socket { get; set; }
        #endregion

        #region Public Methods and Operators
        public void Close()
        {
            // Close the Sockets if they exist
            this.Socket?.Dispose();
        }

        public void Dispose()
        {
            this.Close();
        }

        public void Open()
        {
            // Clear out any latent connections
            this.Close();

            // Set up the main IRC Socket
            this.Socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            var ircConnectArgs = new SocketAsyncEventArgs();
            ircConnectArgs.Completed += this.ConnectionMadeHandler;
            this.Socket.Bind(new IPEndPoint(IPAddress.Any, this.Port));
            this.Socket.Listen(Backlog);

            // Connect to the server
            if (!this.Socket.AcceptAsync(ircConnectArgs))
            {
                this.ConnectionMadeHandler(this, ircConnectArgs);
            }
        }

        public bool SendMessage(Socket socket, string message)
        {
            // Only call if the message exists
            if (string.IsNullOrWhiteSpace(message))
            {
                return false;
            }

            // Cleanup the message
            message = message.EndsWith("\r\n") ? message : $"{message}\r\n";

            // Set up the socket for sending
            var ircMessageArgs = new SocketAsyncEventArgs { RemoteEndPoint = socket.RemoteEndPoint };
            var buffer = Encoding.ASCII.GetBytes(message);
            ircMessageArgs.SetBuffer(buffer, 0, buffer.Length);
            ircMessageArgs.Completed += this.SentHandler;

            // Send
            if (!socket.SendAsync(ircMessageArgs))
            {
                this.SentHandler(null, ircMessageArgs);
            }
            else
            {
                return false;
            }

            return true;
        }
        #endregion

        #region Methods
        protected void ConnectionMadeHandler(object sender, SocketAsyncEventArgs args)
        {
            var connectionSuccess = (args.LastOperation == SocketAsyncOperation.Accept
                                     && args.SocketError == SocketError.Success);

            // If we connected, spin up the message queue
            if (connectionSuccess)
            {
                args.SetBuffer(new byte[BufferSize], 0, BufferSize);
                args.Completed -= this.ConnectionMadeHandler;
                args.Completed += this.ReceivedHandler;
                if (!args.AcceptSocket?.ReceiveAsync(args) ?? false)
                {
                    this.ReceivedHandler(this, args);
                }
            }

            this.ConnectionHandler?.Invoke(this, args);
        }

        protected void ParseMessages(SocketAsyncEventArgs args)
        {
            // Get all of the messages in the queue
            var msgs = this.CurrentInput.Split('\n');

            // For each one that ends in \r, notify people
            foreach (var msg in msgs.Where(msg => msg.EndsWith("\r")))
            {
                this.MessageReceivedHandler?.Invoke(this, msg.TrimEnd('\r'));
                this.AdvancedMessageHandler?.Invoke(
                                                    this,
                                                    new MockArgs
                                                        {
                                                            Message = msg.TrimEnd('\r'),
                                                            Socket = args.AcceptSocket,
                                                            Server = this
                                                        });
            }

            // If the last message didn't end in \r, keep it
            this.CurrentInput = "";
            if (!msgs[msgs.Length - 1].EndsWith("\r"))
            {
                this.CurrentInput = msgs[msgs.Length - 1];
            }
        }

        protected void ReceivedHandler(object sender, SocketAsyncEventArgs args)
        {
            // If there was no data recieved, the connection has been closed
            if (args.BytesTransferred == 0)
            {
                // Cleanup the connection
                this.Close();

                this.ConnectionHandler?.Invoke(this, args);
            }
            else // Data was recieved
            {
                // Recieve the new data
                var tempBuffer = new byte[args.BytesTransferred];
                Array.Copy(args.Buffer, tempBuffer, args.BytesTransferred);
                this.CurrentInput += Encoding.ASCII.GetString(tempBuffer).Trim('\0');

                this.ParseMessages(args);

                // Ask for more data
                try
                {
                    args.SetBuffer(0, BufferSize);
                    if (!args.AcceptSocket.ReceiveAsync(args))
                    {
                        this.ReceivedHandler(this, args);
                    }
                }
                catch (ObjectDisposedException)
                {
                    Debug.WriteLine("Socket has been closed");
                }
            }
        }

        protected void SentHandler(object sender, SocketAsyncEventArgs args)
        {
            // If there was no data sent, the connection has been closed
            if (args.BytesTransferred == 0)
            {
                // Cleanup the connection
                this.Close();

                this.ConnectionHandler?.Invoke(this, args);
            }
        }
        #endregion
    }
}