﻿namespace UniversalUnitTests
{
    #region
    using System;
    using System.Linq;

    using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

    using OneIRC.IRC.Messages;
    using OneIRC.IRC.Messages.Numbers;

    #endregion

    [TestClass]
    public class NumericMessageTests
    {
        #region 353
        [TestMethod]
        [TestCategory("353 Message")]
        [TestCategory("Numeric Message")]
        public void Test353Single()
        {
            var raw = ":kornbluth.freenode.net 353 testnick6 @ #testroom6 :testnick6";
            var msg = IRCMessage.Parse(raw) as Num353Message;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.TargetNickName, "testnick6");
            Assert.AreEqual(msg.ChannelType, ChannelType.Secret);
            CollectionAssert.AreEqual(msg.Targets.ToArray(), new[] { "#testroom6" });
            CollectionAssert.AreEqual(msg.Nicks.ToArray(), new[] { "testnick6" });
        }

        [TestMethod]
        [TestCategory("353 Message")]
        [TestCategory("Numeric Message")]
        public void Test353Many()
        {
            var raw = ":kornbluth.freenode.net 353 testnick6 = #testroom6 :testnick6 testnick8 testnick7";
            var msg = IRCMessage.Parse(raw) as Num353Message;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.TargetNickName, "testnick6");
            Assert.AreEqual(msg.ChannelType, ChannelType.Public);
            CollectionAssert.AreEqual(msg.Targets.ToArray(), new[] { "#testroom6" });
            CollectionAssert.AreEqual(msg.Nicks.ToArray(), new[] { "testnick6", "testnick8", "testnick7" });
        }

        [TestMethod]
        [TestCategory("353 Message")]
        [TestCategory("Numeric Message")]
        public void Test353ConstructorSingle()
        {
            var msg = new Num353Message(
                "testnick6",
                ChannelType.Private,
                "#testroom6",
                new[] { "testnick6" },
                Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "353 testnick6 * #testroom6 :testnick6");
        }

        [TestMethod]
        [TestCategory("353 Message")]
        [TestCategory("Numeric Message")]
        public void Test353ConstructorMany()
        {
            var msg = new Num353Message(
                "testnick6",
                ChannelType.Secret,
                "#testroom6",
                new[] { "testnick6", "testnick8", "testnick7" },
                Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "353 testnick6 @ #testroom6 :testnick6 testnick8 testnick7");
        }
        #endregion

        #region 366
        [TestMethod]
        [TestCategory("366 Message")]
        [TestCategory("Numeric Message")]
        public void Test366()
        {
            var raw = ":kornbluth.freenode.net 366 testnick6 #testroom6 :End of /NAMES list.";
            var msg = IRCMessage.Parse(raw) as Num366Message;

            // Check existance
            Assert.IsNotNull(msg);

            // Check values
            Assert.AreEqual(msg.TargetNickName, "testnick6");
            CollectionAssert.AreEqual(msg.Targets.ToArray(), new[] { "#testroom6" });
        }

        [TestMethod]
        [TestCategory("366 Message")]
        [TestCategory("Numeric Message")]
        public void Test366Constructor()
        {
            var msg = new Num366Message("testnick6", "#testroom6", Guid.NewGuid());

            Assert.AreEqual(msg.RawMessage, "366 testnick6 #testroom6 :End of /NAMES list.");
        }
        #endregion
    }
}