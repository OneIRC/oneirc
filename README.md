# OneIRC #

### What is this repository for? ###

* Many developers use IRC
* IRC on windows is subpar at best, especially compared to *NIX
* The tag-line says it best: Simple, Beautiful, Open-Source IRC for Windows

### How do I get set up? ###

* Runs in VS2015
* Requires Windows 10
* Should run on x86-64 and ARM
* Tests use MSTest built in
* Can be side-loaded, deployment to the store is a manual process

### Contribution guidelines ###

* Back-end work requires tests
* All work must be done in feature branches
* To get changes into master, first open a PR into develop and get that approved

### Who do I talk to? ###

* Zach, if old protocols are your thing